# RusticiSoftwareCloudV2::XapiApi

All URIs are relative to *http://http:/api/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_statement_pipe**](XapiApi.md#create_statement_pipe) | **POST** /xapi/statementPipes | Create an xAPI statement pipe.
[**create_xapi_credential**](XapiApi.md#create_xapi_credential) | **POST** /xapi/credentials | Create an xAPI credential
[**delete_statement_pipe**](XapiApi.md#delete_statement_pipe) | **DELETE** /xapi/statementPipes/{statementPipeId} | Deletes the xAPI statement pipe specified with the id &#x60;statementPipeId&#x60;
[**delete_xapi_credential**](XapiApi.md#delete_xapi_credential) | **DELETE** /xapi/credentials/{xapiCredentialId} | Deletes the xAPI credential specified with the id &#x60;xapiCredentialId&#x60;
[**get_statement_pipe**](XapiApi.md#get_statement_pipe) | **GET** /xapi/statementPipes/{statementPipeId} | Retrieves xAPI statement pipe specified by id &#x60;statementPipeId.&#x60;
[**get_statement_pipes**](XapiApi.md#get_statement_pipes) | **GET** /xapi/statementPipes | Get a list of all xAPI statement pipes
[**get_xapi_credential**](XapiApi.md#get_xapi_credential) | **GET** /xapi/credentials/{xapiCredentialId} | Retrieves the xAPI credential specified by id &#x60;xapiCredentialId&#x60;
[**get_xapi_credentials**](XapiApi.md#get_xapi_credentials) | **GET** /xapi/credentials | Get a list of all xAPI credentials
[**set_statement_pipe**](XapiApi.md#set_statement_pipe) | **PUT** /xapi/statementPipes/{statementPipeId} | Either edits an existing xAPI statement pipe or creates a new one, specified by id &#x60;statementPipeId&#x60;
[**set_xapi_credential**](XapiApi.md#set_xapi_credential) | **PUT** /xapi/credentials/{xapiCredentialId} | Either edits an existing xAPI credential or creates a new one, specified by id &#x60;xapiCredentialId&#x60;



## create_statement_pipe

> StringResultSchema create_statement_pipe(engine_tenant_name, xapi_statement_pipe)

Create an xAPI statement pipe.

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::XapiApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
xapi_statement_pipe = RusticiSoftwareCloudV2::XapiStatementPipePostSchema.new # XapiStatementPipePostSchema | 

begin
  #Create an xAPI statement pipe.
  result = api_instance.create_statement_pipe(engine_tenant_name, xapi_statement_pipe)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling XapiApi->create_statement_pipe: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **xapi_statement_pipe** | [**XapiStatementPipePostSchema**](XapiStatementPipePostSchema.md)|  | 

### Return type

[**StringResultSchema**](StringResultSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## create_xapi_credential

> StringResultSchema create_xapi_credential(engine_tenant_name, xapi_credential)

Create an xAPI credential

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::XapiApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
xapi_credential = RusticiSoftwareCloudV2::XapiCredentialPostSchema.new # XapiCredentialPostSchema | 

begin
  #Create an xAPI credential
  result = api_instance.create_xapi_credential(engine_tenant_name, xapi_credential)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling XapiApi->create_xapi_credential: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **xapi_credential** | [**XapiCredentialPostSchema**](XapiCredentialPostSchema.md)|  | 

### Return type

[**StringResultSchema**](StringResultSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## delete_statement_pipe

> delete_statement_pipe(engine_tenant_name, statement_pipe_id)

Deletes the xAPI statement pipe specified with the id `statementPipeId`

Caution: avoid re-creating a statement pipe with the same ID quickly after a delete. The old version could still be processing, in which case the new pipe could be updated improperly by the processor.

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::XapiApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
statement_pipe_id = 'statement_pipe_id_example' # String | id for this xAPI statement pipe

begin
  #Deletes the xAPI statement pipe specified with the id `statementPipeId`
  api_instance.delete_statement_pipe(engine_tenant_name, statement_pipe_id)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling XapiApi->delete_statement_pipe: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **statement_pipe_id** | **String**| id for this xAPI statement pipe | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## delete_xapi_credential

> delete_xapi_credential(engine_tenant_name, xapi_credential_id)

Deletes the xAPI credential specified with the id `xapiCredentialId`

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::XapiApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
xapi_credential_id = 'xapi_credential_id_example' # String | id for this xAPI credential

begin
  #Deletes the xAPI credential specified with the id `xapiCredentialId`
  api_instance.delete_xapi_credential(engine_tenant_name, xapi_credential_id)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling XapiApi->delete_xapi_credential: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **xapi_credential_id** | **String**| id for this xAPI credential | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_statement_pipe

> XapiStatementPipeSchema get_statement_pipe(engine_tenant_name, statement_pipe_id)

Retrieves xAPI statement pipe specified by id `statementPipeId.`

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::XapiApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
statement_pipe_id = 'statement_pipe_id_example' # String | id for this xAPI statement pipe

begin
  #Retrieves xAPI statement pipe specified by id `statementPipeId.`
  result = api_instance.get_statement_pipe(engine_tenant_name, statement_pipe_id)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling XapiApi->get_statement_pipe: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **statement_pipe_id** | **String**| id for this xAPI statement pipe | 

### Return type

[**XapiStatementPipeSchema**](XapiStatementPipeSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_statement_pipes

> XapiStatementPipeListSchema get_statement_pipes(engine_tenant_name)

Get a list of all xAPI statement pipes

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::XapiApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request

begin
  #Get a list of all xAPI statement pipes
  result = api_instance.get_statement_pipes(engine_tenant_name)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling XapiApi->get_statement_pipes: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 

### Return type

[**XapiStatementPipeListSchema**](XapiStatementPipeListSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_xapi_credential

> XapiCredentialSchema get_xapi_credential(engine_tenant_name, xapi_credential_id)

Retrieves the xAPI credential specified by id `xapiCredentialId`

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::XapiApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
xapi_credential_id = 'xapi_credential_id_example' # String | id for this xAPI credential

begin
  #Retrieves the xAPI credential specified by id `xapiCredentialId`
  result = api_instance.get_xapi_credential(engine_tenant_name, xapi_credential_id)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling XapiApi->get_xapi_credential: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **xapi_credential_id** | **String**| id for this xAPI credential | 

### Return type

[**XapiCredentialSchema**](XapiCredentialSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_xapi_credentials

> XapiCredentialsListSchema get_xapi_credentials(engine_tenant_name, opts)

Get a list of all xAPI credentials

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::XapiApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
opts = {
  more: 'more_example' # String | Value for this parameter will be provided in the 'more' property of lists, where needed. An opaque value, construction and parsing may change without notice.
}

begin
  #Get a list of all xAPI credentials
  result = api_instance.get_xapi_credentials(engine_tenant_name, opts)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling XapiApi->get_xapi_credentials: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **more** | **String**| Value for this parameter will be provided in the &#39;more&#39; property of lists, where needed. An opaque value, construction and parsing may change without notice. | [optional] 

### Return type

[**XapiCredentialsListSchema**](XapiCredentialsListSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## set_statement_pipe

> set_statement_pipe(engine_tenant_name, statement_pipe_id, xapi_statement_pipe)

Either edits an existing xAPI statement pipe or creates a new one, specified by id `statementPipeId`

Editing a statement pipe will cause it to start over and forward any statements it finds, even if the prior version of the pipe had already forwarded those statements. If the statement pipe being edited is currently being processed, the this request will fail with a status code of 409.

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::XapiApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
statement_pipe_id = 'statement_pipe_id_example' # String | id for this xAPI statement pipe
xapi_statement_pipe = RusticiSoftwareCloudV2::XapiStatementPipeSchema.new # XapiStatementPipeSchema | 

begin
  #Either edits an existing xAPI statement pipe or creates a new one, specified by id `statementPipeId`
  api_instance.set_statement_pipe(engine_tenant_name, statement_pipe_id, xapi_statement_pipe)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling XapiApi->set_statement_pipe: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **statement_pipe_id** | **String**| id for this xAPI statement pipe | 
 **xapi_statement_pipe** | [**XapiStatementPipeSchema**](XapiStatementPipeSchema.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## set_xapi_credential

> set_xapi_credential(engine_tenant_name, xapi_credential_id, xapi_credential)

Either edits an existing xAPI credential or creates a new one, specified by id `xapiCredentialId`

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::XapiApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
xapi_credential_id = 'xapi_credential_id_example' # String | id for this xAPI credential
xapi_credential = RusticiSoftwareCloudV2::XapiCredentialSchema.new # XapiCredentialSchema | 

begin
  #Either edits an existing xAPI credential or creates a new one, specified by id `xapiCredentialId`
  api_instance.set_xapi_credential(engine_tenant_name, xapi_credential_id, xapi_credential)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling XapiApi->set_xapi_credential: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **xapi_credential_id** | **String**| id for this xAPI credential | 
 **xapi_credential** | [**XapiCredentialSchema**](XapiCredentialSchema.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

