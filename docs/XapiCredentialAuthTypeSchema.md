# RusticiSoftwareCloudV2::XapiCredentialAuthTypeSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**xapi_credential_auth_type** | **String** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiCredentialAuthTypeSchema.new(xapi_credential_auth_type: null)
```


