# RusticiSoftwareCloudV2::XapiCredentialSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**name** | **String** |  | 
**info** | **String** |  | [optional] 
**secret** | **String** |  | 
**is_enabled** | **Boolean** |  | 
**auth** | [**XapiCredentialAuthTypeSchema**](XapiCredentialAuthTypeSchema.md) |  | 
**permissions_level** | [**XapiCredentialPermissionsLevelSchema**](XapiCredentialPermissionsLevelSchema.md) |  | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiCredentialSchema.new(id: null,
                                 name: null,
                                 info: null,
                                 secret: null,
                                 is_enabled: null,
                                 auth: null,
                                 permissions_level: null)
```


