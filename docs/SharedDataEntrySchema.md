# RusticiSoftwareCloudV2::SharedDataEntrySchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**value** | **String** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::SharedDataEntrySchema.new(id: null,
                                 value: null)
```


