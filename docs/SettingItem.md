# RusticiSoftwareCloudV2::SettingItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**effective_value_source** | **String** | The source of this effective value, default, fallback, or the level the value was set at. | [optional] 
**metadata** | [**SettingMetadata**](SettingMetadata.md) |  | [optional] 
**effective_value** | **String** | The value of this setting that would be used if read at this level, including defaults, fallback, and values set at less specific levels. | [optional] 
**id** | **String** |  | [optional] 
**explicit_value** | **String** | The value of this setting that is explicitly set at this level. If not present, the setting is not specified at this level. | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::SettingItem.new(effective_value_source: null,
                                 metadata: null,
                                 effective_value: null,
                                 id: null,
                                 explicit_value: null)
```


