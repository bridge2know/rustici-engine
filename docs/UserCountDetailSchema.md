# RusticiSoftwareCloudV2::UserCountDetailSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tenant_name** | **String** |  | [optional] 
**total** | **Integer** |  | 
**dispatched** | **Integer** |  | [optional] 
**non_dispatched** | **Integer** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::UserCountDetailSchema.new(tenant_name: null,
                                 total: null,
                                 dispatched: null,
                                 non_dispatched: null)
```


