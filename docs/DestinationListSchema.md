# RusticiSoftwareCloudV2::DestinationListSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**destinations** | [**Array&lt;DestinationIdSchema&gt;**](DestinationIdSchema.md) |  | 
**more** | **String** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::DestinationListSchema.new(destinations: null,
                                 more: null)
```


