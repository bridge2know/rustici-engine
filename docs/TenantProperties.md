# RusticiSoftwareCloudV2::TenantProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **Boolean** | Is this tenant active (valid for use) ? | [optional] [default to true]

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::TenantProperties.new(active: null)
```


