# RusticiSoftwareCloudV2::IntegerResultSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | **Integer** |  | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::IntegerResultSchema.new(result: null)
```


