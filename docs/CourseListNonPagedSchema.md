# RusticiSoftwareCloudV2::CourseListNonPagedSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**courses** | [**Array&lt;CourseSchema&gt;**](CourseSchema.md) |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::CourseListNonPagedSchema.new(courses: null)
```


