# RusticiSoftwareCloudV2::OAuthCredentialsSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**consumer_key** | **String** | the OAuth consumer key | 
**shared_secret** | **String** | the OAuth shared secret | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::OAuthCredentialsSchema.new(consumer_key: null,
                                 shared_secret: null)
```


