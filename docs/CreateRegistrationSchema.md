# RusticiSoftwareCloudV2::CreateRegistrationSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**course_id** | **String** |  | 
**learner** | [**LearnerSchema**](LearnerSchema.md) |  | 
**registration_id** | **String** |  | 
**for_credit** | **Boolean** |  | [optional] [default to true]
**xapi_registration_id** | **String** |  | [optional] 
**post_back** | [**PostBackSchema**](PostBackSchema.md) |  | [optional] 
**initial_registration_state** | [**RegistrationSchema**](RegistrationSchema.md) |  | [optional] 
**initial_settings** | [**SettingsPostSchema**](SettingsPostSchema.md) |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::CreateRegistrationSchema.new(course_id: null,
                                 learner: null,
                                 registration_id: null,
                                 for_credit: null,
                                 xapi_registration_id: null,
                                 post_back: null,
                                 initial_registration_state: null,
                                 initial_settings: null)
```


