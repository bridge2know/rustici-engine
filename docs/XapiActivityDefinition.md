# RusticiSoftwareCloudV2::XapiActivityDefinition

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **Hash&lt;String, String&gt;** |  | [optional] 
**description** | **Hash&lt;String, String&gt;** |  | [optional] 
**type** | **String** |  | [optional] 
**more_info** | **String** |  | [optional] 
**interaction_type** | **String** |  | [optional] 
**correct_responses_pattern** | **Array&lt;String&gt;** |  | [optional] 
**choices** | [**Array&lt;XapiInteractionComponent&gt;**](XapiInteractionComponent.md) |  | [optional] 
**scale** | [**Array&lt;XapiInteractionComponent&gt;**](XapiInteractionComponent.md) |  | [optional] 
**source** | [**Array&lt;XapiInteractionComponent&gt;**](XapiInteractionComponent.md) |  | [optional] 
**target** | [**Array&lt;XapiInteractionComponent&gt;**](XapiInteractionComponent.md) |  | [optional] 
**steps** | [**Array&lt;XapiInteractionComponent&gt;**](XapiInteractionComponent.md) |  | [optional] 
**extensions** | **Hash&lt;String, Object&gt;** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiActivityDefinition.new(name: null,
                                 description: null,
                                 type: null,
                                 more_info: null,
                                 interaction_type: null,
                                 correct_responses_pattern: null,
                                 choices: null,
                                 scale: null,
                                 source: null,
                                 target: null,
                                 steps: null,
                                 extensions: null)
```


