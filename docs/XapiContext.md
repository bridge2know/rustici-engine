# RusticiSoftwareCloudV2::XapiContext

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**registration** | **String** |  | [optional] 
**instructor** | [**XapiAgentGroup**](XapiAgentGroup.md) |  | [optional] 
**team** | [**XapiAgentGroup**](XapiAgentGroup.md) |  | [optional] 
**context_activities** | [**XapiContextActivity**](XapiContextActivity.md) |  | [optional] 
**revision** | **String** |  | [optional] 
**platform** | **String** |  | [optional] 
**language** | **String** |  | [optional] 
**statement** | [**XapiStatementReference**](XapiStatementReference.md) |  | [optional] 
**extensions** | **Hash&lt;String, Object&gt;** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiContext.new(registration: null,
                                 instructor: null,
                                 team: null,
                                 context_activities: null,
                                 revision: null,
                                 platform: null,
                                 language: null,
                                 statement: null,
                                 extensions: null)
```


