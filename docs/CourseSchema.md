# RusticiSoftwareCloudV2::CourseSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**title** | **String** |  | [optional] 
**xapi_activity_id** | **String** | xAPI activity id associated with this course | [optional] 
**updated** | **DateTime** |  | [optional] 
**web_path** | **String** | The web path at which the course&#39;s contents are hosted. For AICC courses, refer to the href property of the child activities as this value will not be available. | [optional] 
**version** | **Integer** |  | [optional] 
**registration_count** | **Integer** |  | [optional] 
**activity_id** | **String** |  | [optional] 
**course_learning_standard** | **String** |  | [optional] 
**metadata** | [**MetadataSchema**](MetadataSchema.md) |  | [optional] 
**root_activity** | [**CourseActivitySchema**](CourseActivitySchema.md) |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::CourseSchema.new(id: null,
                                 title: null,
                                 xapi_activity_id: null,
                                 updated: null,
                                 web_path: null,
                                 version: null,
                                 registration_count: null,
                                 activity_id: null,
                                 course_learning_standard: null,
                                 metadata: null,
                                 root_activity: null)
```


