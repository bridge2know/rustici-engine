# RusticiSoftwareCloudV2::LinkSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**link** | **String** |  | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::LinkSchema.new(link: null)
```


