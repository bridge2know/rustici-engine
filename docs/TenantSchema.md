# RusticiSoftwareCloudV2::TenantSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | name of the tenant | [optional] 
**active** | **Boolean** | is the tenant active | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::TenantSchema.new(name: null,
                                 active: null)
```


