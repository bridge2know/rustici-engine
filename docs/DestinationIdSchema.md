# RusticiSoftwareCloudV2::DestinationIdSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**data** | [**DestinationSchema**](DestinationSchema.md) |  | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::DestinationIdSchema.new(id: null,
                                 data: null)
```


