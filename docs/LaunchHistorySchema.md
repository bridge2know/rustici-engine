# RusticiSoftwareCloudV2::LaunchHistorySchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Identifier for the registration associated with this record | [optional] 
**instance** | **Integer** |  | [optional] 
**score** | [**ScoreSchema**](ScoreSchema.md) |  | [optional] 
**completion_status** | **String** |  | [optional] [default to &#39;UNKNOWN&#39;]
**success_status** | **String** |  | [optional] [default to &#39;UNKNOWN&#39;]
**history_log** | **String** |  | [optional] 
**total_seconds_tracked** | **Float** |  | [optional] 
**launch_time** | **DateTime** | The time of the launch in UTC | [optional] 
**exit_time** | **DateTime** | The time of the exit in UTC | [optional] 
**last_runtime_update** | **DateTime** | The time of the last runtime update in UTC | [optional] 
**launch_history_id** | **String** | A unique identifier for this launch history record | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::LaunchHistorySchema.new(id: null,
                                 instance: null,
                                 score: null,
                                 completion_status: null,
                                 success_status: null,
                                 history_log: null,
                                 total_seconds_tracked: null,
                                 launch_time: null,
                                 exit_time: null,
                                 last_runtime_update: null,
                                 launch_history_id: null)
```


