# RusticiSoftwareCloudV2::LtiReporterSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sourced_id** | **String** | The LTI sourced ID associated with the launch | 
**version** | **String** | The version of LTI reporting to use (1.0 or 1.1). | 
**outcome_url** | **String** | The URL to which LTI results will be posted. | 
**credentials** | [**OAuthCredentialsSchema**](OAuthCredentialsSchema.md) |  | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::LtiReporterSchema.new(sourced_id: null,
                                 version: null,
                                 outcome_url: null,
                                 credentials: null)
```


