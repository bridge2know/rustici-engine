# RusticiSoftwareCloudV2::XapiStatementPipePostSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source** | [**XapiEndpointSchema**](XapiEndpointSchema.md) |  | [optional] 
**target** | [**XapiEndpointSchema**](XapiEndpointSchema.md) |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiStatementPipePostSchema.new(source: null,
                                 target: null)
```


