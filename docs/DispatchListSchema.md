# RusticiSoftwareCloudV2::DispatchListSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dispatches** | [**Array&lt;DispatchIdSchema&gt;**](DispatchIdSchema.md) |  | [optional] 
**more** | **String** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::DispatchListSchema.new(dispatches: null,
                                 more: null)
```


