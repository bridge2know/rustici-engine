# RusticiSoftwareCloudV2::CreateDispatchListSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dispatches** | [**Array&lt;CreateDispatchIdSchema&gt;**](CreateDispatchIdSchema.md) |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::CreateDispatchListSchema.new(dispatches: null)
```


