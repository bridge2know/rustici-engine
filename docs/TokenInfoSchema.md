# RusticiSoftwareCloudV2::TokenInfoSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**permissions** | [**PermissionsSchema**](PermissionsSchema.md) |  | [optional] 
**expiry** | **DateTime** | Expiration of the token. | [optional] 
**additional_values** | [**Array&lt;ItemValuePairSchema&gt;**](ItemValuePairSchema.md) | Additional values that were included in the token | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::TokenInfoSchema.new(permissions: null,
                                 expiry: null,
                                 additional_values: null)
```


