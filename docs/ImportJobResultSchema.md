# RusticiSoftwareCloudV2::ImportJobResultSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**job_id** | **String** |  | [optional] 
**status** | **String** |  | [optional] 
**message** | **String** |  | [optional] 
**import_result** | [**ImportResultSchema**](ImportResultSchema.md) |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::ImportJobResultSchema.new(job_id: null,
                                 status: null,
                                 message: null,
                                 import_result: null)
```


