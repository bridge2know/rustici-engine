# RusticiSoftwareCloudV2::LaunchHistoryListSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**launch_history** | [**Array&lt;LaunchHistorySchema&gt;**](LaunchHistorySchema.md) |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::LaunchHistoryListSchema.new(launch_history: null)
```


