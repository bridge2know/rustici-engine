# RusticiSoftwareCloudV2::XapiAttachment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**usage_type** | **String** |  | 
**display** | **Hash&lt;String, String&gt;** |  | 
**description** | **Hash&lt;String, String&gt;** |  | [optional] 
**content_type** | **String** |  | 
**length** | **Integer** |  | 
**sha2** | **String** |  | 
**file_url** | **String** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiAttachment.new(usage_type: null,
                                 display: null,
                                 description: null,
                                 content_type: null,
                                 length: null,
                                 sha2: null,
                                 file_url: null)
```


