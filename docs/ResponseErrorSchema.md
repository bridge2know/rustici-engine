# RusticiSoftwareCloudV2::ResponseErrorSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **String** |  | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::ResponseErrorSchema.new(message: null)
```


