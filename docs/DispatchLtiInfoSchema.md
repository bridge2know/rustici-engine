# RusticiSoftwareCloudV2::DispatchLtiInfoSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** | The LTI launch URL for this dispatch | 
**consumer_key** | **String** | The OAuth consumer key that identifies the tool consumer for this dispatch. | 
**shared_secret** | **String** | The OAuth secret to be used for LTI authentication for this dispatch. | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::DispatchLtiInfoSchema.new(url: null,
                                 consumer_key: null,
                                 shared_secret: null)
```


