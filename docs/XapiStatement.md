# RusticiSoftwareCloudV2::XapiStatement

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**actor** | [**XapiAgentGroup**](XapiAgentGroup.md) |  | [optional] 
**verb** | [**XapiVerb**](XapiVerb.md) |  | [optional] 
**object_activity** | [**XapiActivity**](XapiActivity.md) |  | [optional] 
**object_agent_group** | [**XapiAgentGroup**](XapiAgentGroup.md) |  | [optional] 
**object_statement_reference** | [**XapiStatementReference**](XapiStatementReference.md) |  | [optional] 
**object_sub_statement** | [**XapiStatement**](XapiStatement.md) |  | [optional] 
**result** | [**XapiResult**](XapiResult.md) |  | [optional] 
**context** | [**XapiContext**](XapiContext.md) |  | [optional] 
**timestamp** | **DateTime** |  | [optional] 
**stored** | **DateTime** |  | [optional] 
**authority** | [**XapiAgentGroup**](XapiAgentGroup.md) |  | [optional] 
**attachments** | [**Array&lt;XapiAttachment&gt;**](XapiAttachment.md) |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiStatement.new(id: null,
                                 actor: null,
                                 verb: null,
                                 object_activity: null,
                                 object_agent_group: null,
                                 object_statement_reference: null,
                                 object_sub_statement: null,
                                 result: null,
                                 context: null,
                                 timestamp: null,
                                 stored: null,
                                 authority: null,
                                 attachments: null)
```


