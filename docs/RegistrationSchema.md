# RusticiSoftwareCloudV2::RegistrationSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**instance** | **Integer** |  | [optional] 
**xapi_registration_id** | **String** | xAPI registration id associated with this registration | [optional] 
**dispatch_id** | **String** | Dispatch ID for this registration, if applicable | [optional] 
**updated** | **DateTime** |  | [optional] 
**registration_completion** | **String** |  | [optional] [default to &#39;UNKNOWN&#39;]
**registration_success** | **String** |  | [optional] [default to &#39;UNKNOWN&#39;]
**score** | [**ScoreSchema**](ScoreSchema.md) |  | [optional] 
**total_seconds_tracked** | **Float** |  | [optional] 
**first_access_date** | **DateTime** |  | [optional] 
**last_access_date** | **DateTime** |  | [optional] 
**completed_date** | **DateTime** |  | [optional] 
**created_date** | **DateTime** |  | [optional] 
**course** | [**CourseReferenceSchema**](CourseReferenceSchema.md) |  | [optional] 
**learner** | [**LearnerSchema**](LearnerSchema.md) |  | [optional] 
**global_objectives** | [**Array&lt;ObjectiveSchema&gt;**](ObjectiveSchema.md) |  | [optional] 
**activity_details** | [**ActivityResultSchema**](ActivityResultSchema.md) |  | [optional] 
**shared_data** | [**Array&lt;SharedDataEntrySchema&gt;**](SharedDataEntrySchema.md) |  | [optional] 
**suspended_activity_id** | **String** |  | [optional] 
**registration_completion_amount** | **Float** | A decimal value between 0 and 1 representing the percentage of this course that the learner has completed so far, if known. Note: for learning standards other than SCORM 2004 4th Edition, this value is based on the percentage of activities completed/passed. This means that single-activity courses in those standards will always return either 0 or 1.  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::RegistrationSchema.new(id: null,
                                 instance: null,
                                 xapi_registration_id: null,
                                 dispatch_id: null,
                                 updated: null,
                                 registration_completion: null,
                                 registration_success: null,
                                 score: null,
                                 total_seconds_tracked: null,
                                 first_access_date: null,
                                 last_access_date: null,
                                 completed_date: null,
                                 created_date: null,
                                 course: null,
                                 learner: null,
                                 global_objectives: null,
                                 activity_details: null,
                                 shared_data: null,
                                 suspended_activity_id: null,
                                 registration_completion_amount: null)
```


