# RusticiSoftwareCloudV2::RegistrationListSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**registrations** | [**Array&lt;RegistrationSchema&gt;**](RegistrationSchema.md) |  | 
**more** | **String** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::RegistrationListSchema.new(registrations: null,
                                 more: null)
```


