# RusticiSoftwareCloudV2::XapiStatementPipeListSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**xapi_statement_pipes** | [**Array&lt;XapiStatementPipeSchema&gt;**](XapiStatementPipeSchema.md) |  | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiStatementPipeListSchema.new(xapi_statement_pipes: null)
```


