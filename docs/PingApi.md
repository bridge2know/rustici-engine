# RusticiSoftwareCloudV2::PingApi

All URIs are relative to *http://http:/api/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ping**](PingApi.md#ping) | **GET** /ping | Get back a message indicating that the API is working.



## ping

> PingSchema ping(opts)

Get back a message indicating that the API is working.

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::PingApi.new
opts = {
  engine_tenant_name: 'engine_tenant_name_example' # String | optional tenant for this request
}

begin
  #Get back a message indicating that the API is working.
  result = api_instance.ping(opts)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling PingApi->ping: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| optional tenant for this request | [optional] 

### Return type

[**PingSchema**](PingSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

