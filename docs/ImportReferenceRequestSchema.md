# RusticiSoftwareCloudV2::ImportReferenceRequestSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** | URL path to the manifest that defines this course | 
**web_path_to_course** | **String** | This is the URL to the root of the course, where the course content is already available. | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::ImportReferenceRequestSchema.new(url: null,
                                 web_path_to_course: null)
```


