# RusticiSoftwareCloudV2::XapiStatementReference

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**object_type** | **String** |  | [default to &#39;StatementRef&#39;]
**id** | **String** |  | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiStatementReference.new(object_type: null,
                                 id: null)
```


