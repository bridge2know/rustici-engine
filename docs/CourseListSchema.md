# RusticiSoftwareCloudV2::CourseListSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**courses** | [**Array&lt;CourseSchema&gt;**](CourseSchema.md) |  | [optional] 
**more** | **String** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::CourseListSchema.new(courses: null,
                                 more: null)
```


