# RusticiSoftwareCloudV2::StaticPropertiesSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**completion_threshold** | **String** |  | [optional] 
**launch_data** | **String** |  | [optional] 
**max_time_allowed** | **String** |  | [optional] 
**scaled_passing_score** | **Float** |  | [optional] 
**scaled_passing_score_used** | **Boolean** |  | [optional] 
**time_limit_action** | **String** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::StaticPropertiesSchema.new(completion_threshold: null,
                                 launch_data: null,
                                 max_time_allowed: null,
                                 scaled_passing_score: null,
                                 scaled_passing_score_used: null,
                                 time_limit_action: null)
```


