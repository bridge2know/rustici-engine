# RusticiSoftwareCloudV2::TokenRequestSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**permissions** | [**PermissionsSchema**](PermissionsSchema.md) |  | 
**expiry** | **DateTime** | Expiration of the token. This should not be set far in the future, as there is no way to invalidate an individual token. | 
**additional_values** | [**Array&lt;ItemValuePairSchema&gt;**](ItemValuePairSchema.md) | Additional values to be included in the token | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::TokenRequestSchema.new(permissions: null,
                                 expiry: null,
                                 additional_values: null)
```


