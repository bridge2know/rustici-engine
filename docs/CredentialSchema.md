# RusticiSoftwareCloudV2::CredentialSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | id for this credential | [optional] 
**name** | **String** | name for this credential | [optional] 
**permissions** | [**PermissionsSchema**](PermissionsSchema.md) |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::CredentialSchema.new(id: null,
                                 name: null,
                                 permissions: null)
```


