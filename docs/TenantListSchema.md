# RusticiSoftwareCloudV2::TenantListSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tenants** | [**Array&lt;TenantSchema&gt;**](TenantSchema.md) |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::TenantListSchema.new(tenants: null)
```


