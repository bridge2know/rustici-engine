# RusticiSoftwareCloudV2::XapiContextActivity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parent** | [**Array&lt;XapiActivity&gt;**](XapiActivity.md) |  | [optional] 
**grouping** | [**Array&lt;XapiActivity&gt;**](XapiActivity.md) |  | [optional] 
**category** | [**Array&lt;XapiActivity&gt;**](XapiActivity.md) |  | [optional] 
**other** | [**Array&lt;XapiActivity&gt;**](XapiActivity.md) |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiContextActivity.new(parent: null,
                                 grouping: null,
                                 category: null,
                                 other: null)
```


