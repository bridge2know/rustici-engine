# RusticiSoftwareCloudV2::SettingMetadata

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**default** | **String** | Default value of this setting | [optional] 
**setting_description** | **String** | description of this setting | [optional] 
**level** | **String** | The level this setting will be applied at, which limits where it can be set. For example, WebPathToContentRoot is applied at the tenant level, so it&#39;s not valid to set it for a registration. | [optional] 
**data_type** | **String** | The data type of this setting | [optional] 
**learning_standards** | **Array&lt;String&gt;** | The list of learning standards this setting applies to. If not present, this setting is not limited to certain learning standards. | [optional] 
**valid_values** | [**Array&lt;SettingValidValue&gt;**](SettingValidValue.md) | For settings with a fixed list of valid values, the list of those values | [optional] 
**fallback** | **String** | A setting that will be used instead of this setting if no value is provided for this setting (This is similar to a default, except that the the value of another setting is being used instead of a fixed default value) | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::SettingMetadata.new(default: null,
                                 setting_description: null,
                                 level: null,
                                 data_type: null,
                                 learning_standards: null,
                                 valid_values: null,
                                 fallback: null)
```


