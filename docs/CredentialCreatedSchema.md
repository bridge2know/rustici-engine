# RusticiSoftwareCloudV2::CredentialCreatedSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | id for this credential | [optional] 
**name** | **String** | name for this credential | [optional] 
**secret** | **String** | Secret for this credential. Only available upon creation. | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::CredentialCreatedSchema.new(id: null,
                                 name: null,
                                 secret: null)
```


