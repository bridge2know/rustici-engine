# RusticiSoftwareCloudV2::DispatchApi

All URIs are relative to *http://http:/api/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_destinations**](DispatchApi.md#create_destinations) | **POST** /dispatch/destinations | Create multiple destinations
[**create_dispatches**](DispatchApi.md#create_dispatches) | **POST** /dispatch/dispatches | Create multiple dispatches
[**delete_destination**](DispatchApi.md#delete_destination) | **DELETE** /dispatch/destinations/{destinationId} | Delete the destination with id &#x60;destinationId&#x60;
[**delete_destination_dispatches**](DispatchApi.md#delete_destination_dispatches) | **DELETE** /dispatch/destinations/{destinationId}/dispatches | Delete all dispatches associated with this destination
[**delete_dispatch**](DispatchApi.md#delete_dispatch) | **DELETE** /dispatch/dispatches/{dispatchId} | Delete the dispatch with id &#x60;dispatchId&#x60;
[**enable_registration_instancing**](DispatchApi.md#enable_registration_instancing) | **POST** /dispatch/destinations/{destinationId}/dispatches/registrationInstancing | Enable or disable registration instancing
[**get_destination**](DispatchApi.md#get_destination) | **GET** /dispatch/destinations/{destinationId} | Get the destination with id &#x60;destinationId&#x60;
[**get_destination_dispatch_registration_count**](DispatchApi.md#get_destination_dispatch_registration_count) | **GET** /dispatch/destinations/{destinationId}/registrationCount | Get the registration count for all related dispatch registrations
[**get_destination_dispatch_zip**](DispatchApi.md#get_destination_dispatch_zip) | **GET** /dispatch/destinations/{destinationId}/dispatches/zip | Get a ZIP file containing all dispatch packages related to a destination.
[**get_destination_dispatches**](DispatchApi.md#get_destination_dispatches) | **GET** /dispatch/destinations/{destinationId}/dispatches | Get a list of related dispatches
[**get_destinations**](DispatchApi.md#get_destinations) | **GET** /dispatch/destinations | Get a list of destinations
[**get_dispatch**](DispatchApi.md#get_dispatch) | **GET** /dispatch/dispatches/{dispatchId} | Get the dispatch with id &#x60;dispatchId&#x60;
[**get_dispatch_enabled**](DispatchApi.md#get_dispatch_enabled) | **GET** /dispatch/dispatches/{dispatchId}/enabled | Returns boolean indicating if dispatch with id &#x60;dispatchId&#x60; is enabled
[**get_dispatch_registration_count**](DispatchApi.md#get_dispatch_registration_count) | **GET** /dispatch/dispatches/{dispatchId}/registrationCount | Get the registration count for this dispatch, and the date and time of the last count reset, if any.
[**get_dispatch_zip**](DispatchApi.md#get_dispatch_zip) | **GET** /dispatch/dispatches/{dispatchId}/zip | Get the ZIP dispatch package.
[**get_dispatches**](DispatchApi.md#get_dispatches) | **GET** /dispatch/dispatches | Get a list of dispatches
[**get_lti_dispatch**](DispatchApi.md#get_lti_dispatch) | **GET** /dispatch/dispatches/{dispatchId}/lti | Get the information necessary to launch this dispatch using the IMS LTI specification.
[**post_dispatch_lti_reporters**](DispatchApi.md#post_dispatch_lti_reporters) | **POST** /dispatch/ltiReporters | Set up a temporary LTI reporter; for use by products that use their own LTI entry points
[**reset_destination_dispatch_registration_count**](DispatchApi.md#reset_destination_dispatch_registration_count) | **DELETE** /dispatch/destinations/{destinationId}/registrationCount | Reset the registration count for related dispatches.
[**reset_dispatch_registration_count**](DispatchApi.md#reset_dispatch_registration_count) | **DELETE** /dispatch/dispatches/{dispatchId}/registrationCount | Reset the registration count for this dispatch.
[**set_destination**](DispatchApi.md#set_destination) | **PUT** /dispatch/destinations/{destinationId} | Create or update the destination with id &#x60;destinationId&#x60;
[**set_destination_dispatch_enabled**](DispatchApi.md#set_destination_dispatch_enabled) | **POST** /dispatch/destinations/{destinationId}/dispatches/enabled | Enable or disable all related dispatches
[**set_dispatch_enabled**](DispatchApi.md#set_dispatch_enabled) | **PUT** /dispatch/dispatches/{dispatchId}/enabled | Enable or disable the dispatch
[**update_dispatch**](DispatchApi.md#update_dispatch) | **PUT** /dispatch/dispatches/{dispatchId} | Update the dispatch with id &#x60;dispatchId&#x60;



## create_destinations

> create_destinations(engine_tenant_name, destinations_list)

Create multiple destinations

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
destinations_list = RusticiSoftwareCloudV2::DestinationListSchema.new # DestinationListSchema | 

begin
  #Create multiple destinations
  api_instance.create_destinations(engine_tenant_name, destinations_list)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->create_destinations: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **destinations_list** | [**DestinationListSchema**](DestinationListSchema.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## create_dispatches

> create_dispatches(engine_tenant_name, dispatch_list)

Create multiple dispatches

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
dispatch_list = RusticiSoftwareCloudV2::CreateDispatchListSchema.new # CreateDispatchListSchema | 

begin
  #Create multiple dispatches
  api_instance.create_dispatches(engine_tenant_name, dispatch_list)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->create_dispatches: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **dispatch_list** | [**CreateDispatchListSchema**](CreateDispatchListSchema.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## delete_destination

> delete_destination(engine_tenant_name, destination_id)

Delete the destination with id `destinationId`

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
destination_id = 'destination_id_example' # String | 

begin
  #Delete the destination with id `destinationId`
  api_instance.delete_destination(engine_tenant_name, destination_id)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->delete_destination: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **destination_id** | **String**|  | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## delete_destination_dispatches

> delete_destination_dispatches(engine_tenant_name, destination_id)

Delete all dispatches associated with this destination

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
destination_id = 'destination_id_example' # String | 

begin
  #Delete all dispatches associated with this destination
  api_instance.delete_destination_dispatches(engine_tenant_name, destination_id)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->delete_destination_dispatches: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **destination_id** | **String**|  | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## delete_dispatch

> delete_dispatch(engine_tenant_name, dispatch_id)

Delete the dispatch with id `dispatchId`

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
dispatch_id = 'dispatch_id_example' # String | 

begin
  #Delete the dispatch with id `dispatchId`
  api_instance.delete_dispatch(engine_tenant_name, dispatch_id)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->delete_dispatch: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **dispatch_id** | **String**|  | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## enable_registration_instancing

> enable_registration_instancing(engine_tenant_name, destination_id, enabled)

Enable or disable registration instancing

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
destination_id = 'destination_id_example' # String | 
enabled = RusticiSoftwareCloudV2::EnabledSchema.new # EnabledSchema | 

begin
  #Enable or disable registration instancing
  api_instance.enable_registration_instancing(engine_tenant_name, destination_id, enabled)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->enable_registration_instancing: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **destination_id** | **String**|  | 
 **enabled** | [**EnabledSchema**](EnabledSchema.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## get_destination

> DestinationSchema get_destination(engine_tenant_name, destination_id)

Get the destination with id `destinationId`

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
destination_id = 'destination_id_example' # String | 

begin
  #Get the destination with id `destinationId`
  result = api_instance.get_destination(engine_tenant_name, destination_id)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->get_destination: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **destination_id** | **String**|  | 

### Return type

[**DestinationSchema**](DestinationSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_destination_dispatch_registration_count

> IntegerResultSchema get_destination_dispatch_registration_count(engine_tenant_name, destination_id)

Get the registration count for all related dispatch registrations

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
destination_id = 'destination_id_example' # String | 

begin
  #Get the registration count for all related dispatch registrations
  result = api_instance.get_destination_dispatch_registration_count(engine_tenant_name, destination_id)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->get_destination_dispatch_registration_count: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **destination_id** | **String**|  | 

### Return type

[**IntegerResultSchema**](IntegerResultSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_destination_dispatch_zip

> File get_destination_dispatch_zip(engine_tenant_name, destination_id, opts)

Get a ZIP file containing all dispatch packages related to a destination.

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
destination_id = 'destination_id_example' # String | 
opts = {
  type: '\"SCORM12\"' # String | The type of dispatch package to export (SCORM12, SCORM2004-3RD or AICC)
}

begin
  #Get a ZIP file containing all dispatch packages related to a destination.
  result = api_instance.get_destination_dispatch_zip(engine_tenant_name, destination_id, opts)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->get_destination_dispatch_zip: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **destination_id** | **String**|  | 
 **type** | **String**| The type of dispatch package to export (SCORM12, SCORM2004-3RD or AICC) | [optional] [default to &#39;\&quot;SCORM12\&quot;&#39;]

### Return type

**File**

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/zip


## get_destination_dispatches

> DispatchListSchema get_destination_dispatches(engine_tenant_name, destination_id, opts)

Get a list of related dispatches

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
destination_id = 'destination_id_example' # String | 
opts = {
  more: 'more_example', # String | Value for this parameter will be provided in the 'more' property of lists, where needed. An opaque value, construction and parsing may change without notice.
  since: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Only items updated since the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  _until: DateTime.parse('2013-10-20T19:20:30+01:00') # DateTime | Only items updated up until the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
}

begin
  #Get a list of related dispatches
  result = api_instance.get_destination_dispatches(engine_tenant_name, destination_id, opts)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->get_destination_dispatches: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **destination_id** | **String**|  | 
 **more** | **String**| Value for this parameter will be provided in the &#39;more&#39; property of lists, where needed. An opaque value, construction and parsing may change without notice. | [optional] 
 **since** | **DateTime**| Only items updated since the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used. | [optional] 
 **_until** | **DateTime**| Only items updated up until the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used. | [optional] 

### Return type

[**DispatchListSchema**](DispatchListSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_destinations

> DestinationListSchema get_destinations(engine_tenant_name, opts)

Get a list of destinations

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
opts = {
  more: 'more_example', # String | Value for this parameter will be provided in the 'more' property of lists, where needed. An opaque value, construction and parsing may change without notice.
  since: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Only items updated since the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  _until: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Only items updated up until the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  course_id: 'course_id_example' # String | Limit the results to destinations that have dispatches of the specified course
}

begin
  #Get a list of destinations
  result = api_instance.get_destinations(engine_tenant_name, opts)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->get_destinations: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **more** | **String**| Value for this parameter will be provided in the &#39;more&#39; property of lists, where needed. An opaque value, construction and parsing may change without notice. | [optional] 
 **since** | **DateTime**| Only items updated since the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used. | [optional] 
 **_until** | **DateTime**| Only items updated up until the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used. | [optional] 
 **course_id** | **String**| Limit the results to destinations that have dispatches of the specified course | [optional] 

### Return type

[**DestinationListSchema**](DestinationListSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_dispatch

> DispatchSchema get_dispatch(engine_tenant_name, dispatch_id)

Get the dispatch with id `dispatchId`

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
dispatch_id = 'dispatch_id_example' # String | 

begin
  #Get the dispatch with id `dispatchId`
  result = api_instance.get_dispatch(engine_tenant_name, dispatch_id)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->get_dispatch: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **dispatch_id** | **String**|  | 

### Return type

[**DispatchSchema**](DispatchSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_dispatch_enabled

> EnabledSchema get_dispatch_enabled(engine_tenant_name, dispatch_id)

Returns boolean indicating if dispatch with id `dispatchId` is enabled

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
dispatch_id = 'dispatch_id_example' # String | 

begin
  #Returns boolean indicating if dispatch with id `dispatchId` is enabled
  result = api_instance.get_dispatch_enabled(engine_tenant_name, dispatch_id)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->get_dispatch_enabled: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **dispatch_id** | **String**|  | 

### Return type

[**EnabledSchema**](EnabledSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_dispatch_registration_count

> DispatchRegistrationCountSchema get_dispatch_registration_count(engine_tenant_name, dispatch_id)

Get the registration count for this dispatch, and the date and time of the last count reset, if any.

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
dispatch_id = 'dispatch_id_example' # String | 

begin
  #Get the registration count for this dispatch, and the date and time of the last count reset, if any.
  result = api_instance.get_dispatch_registration_count(engine_tenant_name, dispatch_id)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->get_dispatch_registration_count: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **dispatch_id** | **String**|  | 

### Return type

[**DispatchRegistrationCountSchema**](DispatchRegistrationCountSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_dispatch_zip

> File get_dispatch_zip(engine_tenant_name, dispatch_id, opts)

Get the ZIP dispatch package.

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
dispatch_id = 'dispatch_id_example' # String | 
opts = {
  type: '\"SCORM12\"' # String | The type of dispatch package to export (SCORM12, SCORM2004-3RD or AICC)
}

begin
  #Get the ZIP dispatch package.
  result = api_instance.get_dispatch_zip(engine_tenant_name, dispatch_id, opts)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->get_dispatch_zip: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **dispatch_id** | **String**|  | 
 **type** | **String**| The type of dispatch package to export (SCORM12, SCORM2004-3RD or AICC) | [optional] [default to &#39;\&quot;SCORM12\&quot;&#39;]

### Return type

**File**

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/zip


## get_dispatches

> DispatchListSchema get_dispatches(engine_tenant_name, opts)

Get a list of dispatches

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
opts = {
  more: 'more_example', # String | Value for this parameter will be provided in the 'more' property of lists, where needed. An opaque value, construction and parsing may change without notice.
  since: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Only items updated since the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  _until: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Only items updated up until the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  course_id: 'course_id_example' # String | Limit the results to dispatches of the specified course
}

begin
  #Get a list of dispatches
  result = api_instance.get_dispatches(engine_tenant_name, opts)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->get_dispatches: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **more** | **String**| Value for this parameter will be provided in the &#39;more&#39; property of lists, where needed. An opaque value, construction and parsing may change without notice. | [optional] 
 **since** | **DateTime**| Only items updated since the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used. | [optional] 
 **_until** | **DateTime**| Only items updated up until the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used. | [optional] 
 **course_id** | **String**| Limit the results to dispatches of the specified course | [optional] 

### Return type

[**DispatchListSchema**](DispatchListSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_lti_dispatch

> DispatchLtiInfoSchema get_lti_dispatch(engine_tenant_name, dispatch_id)

Get the information necessary to launch this dispatch using the IMS LTI specification.

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
dispatch_id = 'dispatch_id_example' # String | 

begin
  #Get the information necessary to launch this dispatch using the IMS LTI specification.
  result = api_instance.get_lti_dispatch(engine_tenant_name, dispatch_id)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->get_lti_dispatch: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **dispatch_id** | **String**|  | 

### Return type

[**DispatchLtiInfoSchema**](DispatchLtiInfoSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## post_dispatch_lti_reporters

> LtiReporterIdSchema post_dispatch_lti_reporters(engine_tenant_name, lti_reporter_schema)

Set up a temporary LTI reporter; for use by products that use their own LTI entry points

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
lti_reporter_schema = RusticiSoftwareCloudV2::LtiReporterSchema.new # LtiReporterSchema | 

begin
  #Set up a temporary LTI reporter; for use by products that use their own LTI entry points
  result = api_instance.post_dispatch_lti_reporters(engine_tenant_name, lti_reporter_schema)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->post_dispatch_lti_reporters: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **lti_reporter_schema** | [**LtiReporterSchema**](LtiReporterSchema.md)|  | 

### Return type

[**LtiReporterIdSchema**](LtiReporterIdSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## reset_destination_dispatch_registration_count

> reset_destination_dispatch_registration_count(engine_tenant_name, destination_id)

Reset the registration count for related dispatches.

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
destination_id = 'destination_id_example' # String | 

begin
  #Reset the registration count for related dispatches.
  api_instance.reset_destination_dispatch_registration_count(engine_tenant_name, destination_id)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->reset_destination_dispatch_registration_count: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **destination_id** | **String**|  | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## reset_dispatch_registration_count

> reset_dispatch_registration_count(engine_tenant_name, dispatch_id)

Reset the registration count for this dispatch.

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
dispatch_id = 'dispatch_id_example' # String | 

begin
  #Reset the registration count for this dispatch.
  api_instance.reset_dispatch_registration_count(engine_tenant_name, dispatch_id)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->reset_dispatch_registration_count: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **dispatch_id** | **String**|  | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## set_destination

> set_destination(engine_tenant_name, destination_id, destination)

Create or update the destination with id `destinationId`

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
destination_id = 'destination_id_example' # String | 
destination = RusticiSoftwareCloudV2::DestinationSchema.new # DestinationSchema | 

begin
  #Create or update the destination with id `destinationId`
  api_instance.set_destination(engine_tenant_name, destination_id, destination)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->set_destination: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **destination_id** | **String**|  | 
 **destination** | [**DestinationSchema**](DestinationSchema.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## set_destination_dispatch_enabled

> set_destination_dispatch_enabled(engine_tenant_name, destination_id, enabled)

Enable or disable all related dispatches

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
destination_id = 'destination_id_example' # String | 
enabled = RusticiSoftwareCloudV2::EnabledSchema.new # EnabledSchema | 

begin
  #Enable or disable all related dispatches
  api_instance.set_destination_dispatch_enabled(engine_tenant_name, destination_id, enabled)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->set_destination_dispatch_enabled: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **destination_id** | **String**|  | 
 **enabled** | [**EnabledSchema**](EnabledSchema.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## set_dispatch_enabled

> set_dispatch_enabled(engine_tenant_name, dispatch_id, enabled)

Enable or disable the dispatch

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
dispatch_id = 'dispatch_id_example' # String | 
enabled = RusticiSoftwareCloudV2::EnabledSchema.new # EnabledSchema | 

begin
  #Enable or disable the dispatch
  api_instance.set_dispatch_enabled(engine_tenant_name, dispatch_id, enabled)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->set_dispatch_enabled: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **dispatch_id** | **String**|  | 
 **enabled** | [**EnabledSchema**](EnabledSchema.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## update_dispatch

> update_dispatch(engine_tenant_name, dispatch_id, dispatch_properties)

Update the dispatch with id `dispatchId`

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::DispatchApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
dispatch_id = 'dispatch_id_example' # String | 
dispatch_properties = RusticiSoftwareCloudV2::UpdateDispatchSchema.new # UpdateDispatchSchema | 

begin
  #Update the dispatch with id `dispatchId`
  api_instance.update_dispatch(engine_tenant_name, dispatch_id, dispatch_properties)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling DispatchApi->update_dispatch: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **dispatch_id** | **String**|  | 
 **dispatch_properties** | [**UpdateDispatchSchema**](UpdateDispatchSchema.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

