# RusticiSoftwareCloudV2::ImportResultSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**web_path_to_course** | **String** | web path to this course | [optional] 
**parser_warnings** | **Array&lt;String&gt;** |  | [optional] 
**course_languages** | **Array&lt;String&gt;** |  | [optional] 
**course** | [**CourseSchema**](CourseSchema.md) |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::ImportResultSchema.new(web_path_to_course: null,
                                 parser_warnings: null,
                                 course_languages: null,
                                 course: null)
```


