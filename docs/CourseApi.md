# RusticiSoftwareCloudV2::CourseApi

All URIs are relative to *http://http:/api/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**build_course_preview_launch_link**](CourseApi.md#build_course_preview_launch_link) | **POST** /courses/{courseId}/preview | Returns the link to use to preview this course
[**build_course_preview_launch_link_with_version**](CourseApi.md#build_course_preview_launch_link_with_version) | **POST** /courses/{courseId}/versions/{versionId}/preview | Returns the link to use to preview this course
[**create_fetch_and_import_course_job**](CourseApi.md#create_fetch_and_import_course_job) | **POST** /courses/importJobs | Start a job to fetch and import a course.
[**create_upload_and_import_course_job**](CourseApi.md#create_upload_and_import_course_job) | **POST** /courses/importJobs/upload | Upload a course and start an import job for it.
[**delete_course**](CourseApi.md#delete_course) | **DELETE** /courses/{courseId} | Delete &#x60;courseId&#x60;
[**delete_course_configuration_setting**](CourseApi.md#delete_course_configuration_setting) | **DELETE** /courses/{courseId}/configuration/{settingId} | Clears the &#x60;settingId&#x60; value for this course
[**delete_course_version**](CourseApi.md#delete_course_version) | **DELETE** /courses/{courseId}/versions/{versionId} | Delete version &#x60;versionId&#x60; of &#x60;courseId&#x60;
[**delete_course_version_configuration_setting**](CourseApi.md#delete_course_version_configuration_setting) | **DELETE** /courses/{courseId}/versions/{versionId}/configuration/{settingId} | Clears the &#x60;settingId&#x60; value for this course and version.
[**get_course**](CourseApi.md#get_course) | **GET** /courses/{courseId} | Get information about &#x60;courseId&#x60;
[**get_course_configuration**](CourseApi.md#get_course_configuration) | **GET** /courses/{courseId}/configuration | Returns all configuration settings for this course
[**get_course_statements**](CourseApi.md#get_course_statements) | **GET** /courses/{courseId}/xAPIStatements | Get xAPI statements for &#x60;courseId&#x60;
[**get_course_version_configuration**](CourseApi.md#get_course_version_configuration) | **GET** /courses/{courseId}/versions/{versionId}/configuration | Returns all configuration settings for this course and version.
[**get_course_version_info**](CourseApi.md#get_course_version_info) | **GET** /courses/{courseId}/versions/{versionId} | Get version &#x60;versionId&#x60; of &#x60;courseId&#x60;
[**get_course_version_statements**](CourseApi.md#get_course_version_statements) | **GET** /courses/{courseId}/versions/{versionId}/xAPIStatements | Get xAPI statements for version &#x60;versionId&#x60; of &#x60;courseId&#x60;
[**get_course_versions**](CourseApi.md#get_course_versions) | **GET** /courses/{courseId}/versions | Get all versions of &#x60;courseId&#x60;
[**get_courses**](CourseApi.md#get_courses) | **GET** /courses | Get a list of all courses for the specified tenant
[**get_import_job_status**](CourseApi.md#get_import_job_status) | **GET** /courses/importJobs/{importJobId} | Check the status of an import job.
[**import_course_without_upload**](CourseApi.md#import_course_without_upload) | **POST** /courses | Create a course
[**set_course_configuration**](CourseApi.md#set_course_configuration) | **POST** /courses/{courseId}/configuration | Set configuration settings for this course.
[**set_course_title**](CourseApi.md#set_course_title) | **PUT** /courses/{courseId}/title | Sets the course title for &#x60;courseId&#x60;
[**set_course_version_configuration**](CourseApi.md#set_course_version_configuration) | **POST** /courses/{courseId}/versions/{versionId}/configuration | Set configuration settings for this course and version.
[**upload_and_import_course**](CourseApi.md#upload_and_import_course) | **POST** /courses/upload | Upload a course to import



## build_course_preview_launch_link

> LaunchLinkSchema build_course_preview_launch_link(engine_tenant_name, course_id, launch_link_request)

Returns the link to use to preview this course

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
course_id = 'course_id_example' # String | 
launch_link_request = RusticiSoftwareCloudV2::LaunchLinkRequestSchema.new # LaunchLinkRequestSchema | 

begin
  #Returns the link to use to preview this course
  result = api_instance.build_course_preview_launch_link(engine_tenant_name, course_id, launch_link_request)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->build_course_preview_launch_link: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **course_id** | **String**|  | 
 **launch_link_request** | [**LaunchLinkRequestSchema**](LaunchLinkRequestSchema.md)|  | 

### Return type

[**LaunchLinkSchema**](LaunchLinkSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## build_course_preview_launch_link_with_version

> LaunchLinkSchema build_course_preview_launch_link_with_version(engine_tenant_name, course_id, version_id, launch_link_request)

Returns the link to use to preview this course

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
course_id = 'course_id_example' # String | 
version_id = 56 # Integer | the course version
launch_link_request = RusticiSoftwareCloudV2::LaunchLinkRequestSchema.new # LaunchLinkRequestSchema | 

begin
  #Returns the link to use to preview this course
  result = api_instance.build_course_preview_launch_link_with_version(engine_tenant_name, course_id, version_id, launch_link_request)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->build_course_preview_launch_link_with_version: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **course_id** | **String**|  | 
 **version_id** | **Integer**| the course version | 
 **launch_link_request** | [**LaunchLinkRequestSchema**](LaunchLinkRequestSchema.md)|  | 

### Return type

[**LaunchLinkSchema**](LaunchLinkSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## create_fetch_and_import_course_job

> StringResultSchema create_fetch_and_import_course_job(engine_tenant_name, course_id, import_request, opts)

Start a job to fetch and import a course.

An import job will be started to fetch and import the referenced file, and the import job ID will be returned. If the import is successful, the imported course will be registered using the courseId provided.

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
course_id = 'course_id_example' # String | A unique identifier your application will use to identify the course after import. Your application is responsible both for generating this unique ID and for keeping track of the ID for later use.
import_request = RusticiSoftwareCloudV2::ImportFetchRequestSchema.new # ImportFetchRequestSchema | 
opts = {
  may_create_new_version: false # Boolean | Is it OK to create a new version of this course? If this is set to false and the course already exists, the upload will fail. If true and the course already exists then a new version will be created. No effect if the course doesn't already exist.
}

begin
  #Start a job to fetch and import a course.
  result = api_instance.create_fetch_and_import_course_job(engine_tenant_name, course_id, import_request, opts)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->create_fetch_and_import_course_job: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **course_id** | **String**| A unique identifier your application will use to identify the course after import. Your application is responsible both for generating this unique ID and for keeping track of the ID for later use. | 
 **import_request** | [**ImportFetchRequestSchema**](ImportFetchRequestSchema.md)|  | 
 **may_create_new_version** | **Boolean**| Is it OK to create a new version of this course? If this is set to false and the course already exists, the upload will fail. If true and the course already exists then a new version will be created. No effect if the course doesn&#39;t already exist. | [optional] [default to false]

### Return type

[**StringResultSchema**](StringResultSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## create_upload_and_import_course_job

> StringResultSchema create_upload_and_import_course_job(engine_tenant_name, course_id, opts)

Upload a course and start an import job for it.

An import job will be started to import the posted file, and the import job ID will be returned. If the import is successful, the imported course will be registered using the courseId provided.

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
course_id = 'course_id_example' # String | A unique identifier your application will use to identify the course after import. Your application is responsible both for generating this unique ID and for keeping track of the ID for later use.
opts = {
  may_create_new_version: false, # Boolean | Is it OK to create a new version of this course? If this is set to false and the course already exists, the upload will fail. If true and the course already exists then a new version will be created. No effect if the course doesn't already exist.
  file: File.new('/path/to/file') # File | The zip file of the course contents to import.
}

begin
  #Upload a course and start an import job for it.
  result = api_instance.create_upload_and_import_course_job(engine_tenant_name, course_id, opts)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->create_upload_and_import_course_job: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **course_id** | **String**| A unique identifier your application will use to identify the course after import. Your application is responsible both for generating this unique ID and for keeping track of the ID for later use. | 
 **may_create_new_version** | **Boolean**| Is it OK to create a new version of this course? If this is set to false and the course already exists, the upload will fail. If true and the course already exists then a new version will be created. No effect if the course doesn&#39;t already exist. | [optional] [default to false]
 **file** | **File**| The zip file of the course contents to import. | [optional] 

### Return type

[**StringResultSchema**](StringResultSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json


## delete_course

> delete_course(engine_tenant_name, course_id)

Delete `courseId`

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
course_id = 'course_id_example' # String | 

begin
  #Delete `courseId`
  api_instance.delete_course(engine_tenant_name, course_id)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->delete_course: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **course_id** | **String**|  | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## delete_course_configuration_setting

> delete_course_configuration_setting(engine_tenant_name, course_id, setting_id)

Clears the `settingId` value for this course

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
course_id = 'course_id_example' # String | 
setting_id = 'setting_id_example' # String | 

begin
  #Clears the `settingId` value for this course
  api_instance.delete_course_configuration_setting(engine_tenant_name, course_id, setting_id)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->delete_course_configuration_setting: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **course_id** | **String**|  | 
 **setting_id** | **String**|  | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## delete_course_version

> delete_course_version(engine_tenant_name, course_id, version_id)

Delete version `versionId` of `courseId`

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
course_id = 'course_id_example' # String | 
version_id = 56 # Integer | the course version

begin
  #Delete version `versionId` of `courseId`
  api_instance.delete_course_version(engine_tenant_name, course_id, version_id)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->delete_course_version: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **course_id** | **String**|  | 
 **version_id** | **Integer**| the course version | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## delete_course_version_configuration_setting

> delete_course_version_configuration_setting(engine_tenant_name, course_id, version_id, setting_id)

Clears the `settingId` value for this course and version.

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
course_id = 'course_id_example' # String | 
version_id = 56 # Integer | the course version
setting_id = 'setting_id_example' # String | 

begin
  #Clears the `settingId` value for this course and version.
  api_instance.delete_course_version_configuration_setting(engine_tenant_name, course_id, version_id, setting_id)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->delete_course_version_configuration_setting: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **course_id** | **String**|  | 
 **version_id** | **Integer**| the course version | 
 **setting_id** | **String**|  | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_course

> CourseSchema get_course(engine_tenant_name, course_id, opts)

Get information about `courseId`

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
course_id = 'course_id_example' # String | 
opts = {
  include_registration_count: false, # Boolean | Include the registration count in the results
  include_course_metadata: false # Boolean | Include course metadata in the results
}

begin
  #Get information about `courseId`
  result = api_instance.get_course(engine_tenant_name, course_id, opts)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->get_course: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **course_id** | **String**|  | 
 **include_registration_count** | **Boolean**| Include the registration count in the results | [optional] [default to false]
 **include_course_metadata** | **Boolean**| Include course metadata in the results | [optional] [default to false]

### Return type

[**CourseSchema**](CourseSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_course_configuration

> SettingListSchema get_course_configuration(engine_tenant_name, course_id, opts)

Returns all configuration settings for this course

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
course_id = 'course_id_example' # String | 
opts = {
  include_metadata: false, # Boolean | 
  include_hidden_settings: false, # Boolean | Should settings that are declared to be hidden be included. Note: such settings generally do not need to be modified, and may be confusing. 
  include_secret_settings: false, # Boolean | Should settings that are stored encrypted (type 'secretString') be included. Note: if included, the decrypted value will be returned. 
  process_replacement_tokens: true # Boolean | Whether to process replacement tokens (false returns the raw value of each setting, without tokens or environment variable replacements)
}

begin
  #Returns all configuration settings for this course
  result = api_instance.get_course_configuration(engine_tenant_name, course_id, opts)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->get_course_configuration: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **course_id** | **String**|  | 
 **include_metadata** | **Boolean**|  | [optional] [default to false]
 **include_hidden_settings** | **Boolean**| Should settings that are declared to be hidden be included. Note: such settings generally do not need to be modified, and may be confusing.  | [optional] [default to false]
 **include_secret_settings** | **Boolean**| Should settings that are stored encrypted (type &#39;secretString&#39;) be included. Note: if included, the decrypted value will be returned.  | [optional] [default to false]
 **process_replacement_tokens** | **Boolean**| Whether to process replacement tokens (false returns the raw value of each setting, without tokens or environment variable replacements) | [optional] [default to true]

### Return type

[**SettingListSchema**](SettingListSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_course_statements

> XapiStatementResult get_course_statements(engine_tenant_name, course_id, opts)

Get xAPI statements for `courseId`

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
course_id = 'course_id_example' # String | 
opts = {
  learner_id: 'learner_id_example', # String | Only entries for the specified learner id will be included.
  since: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Only items updated since the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  _until: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Only items updated up until the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  more: 'more_example' # String | Value for this parameter will be provided in the 'more' property of lists, where needed. An opaque value, construction and parsing may change without notice.
}

begin
  #Get xAPI statements for `courseId`
  result = api_instance.get_course_statements(engine_tenant_name, course_id, opts)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->get_course_statements: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **course_id** | **String**|  | 
 **learner_id** | **String**| Only entries for the specified learner id will be included. | [optional] 
 **since** | **DateTime**| Only items updated since the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used. | [optional] 
 **_until** | **DateTime**| Only items updated up until the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used. | [optional] 
 **more** | **String**| Value for this parameter will be provided in the &#39;more&#39; property of lists, where needed. An opaque value, construction and parsing may change without notice. | [optional] 

### Return type

[**XapiStatementResult**](XapiStatementResult.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_course_version_configuration

> SettingListSchema get_course_version_configuration(engine_tenant_name, course_id, version_id, opts)

Returns all configuration settings for this course and version.

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
course_id = 'course_id_example' # String | 
version_id = 56 # Integer | the course version
opts = {
  include_metadata: false, # Boolean | 
  include_hidden_settings: false, # Boolean | Should settings that are declared to be hidden be included. Note: such settings generally do not need to be modified, and may be confusing. 
  include_secret_settings: false, # Boolean | Should settings that are stored encrypted (type 'secretString') be included. Note: if included, the decrypted value will be returned. 
  process_replacement_tokens: true # Boolean | Whether to process replacement tokens (false returns the raw value of each setting, without tokens or environment variable replacements)
}

begin
  #Returns all configuration settings for this course and version.
  result = api_instance.get_course_version_configuration(engine_tenant_name, course_id, version_id, opts)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->get_course_version_configuration: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **course_id** | **String**|  | 
 **version_id** | **Integer**| the course version | 
 **include_metadata** | **Boolean**|  | [optional] [default to false]
 **include_hidden_settings** | **Boolean**| Should settings that are declared to be hidden be included. Note: such settings generally do not need to be modified, and may be confusing.  | [optional] [default to false]
 **include_secret_settings** | **Boolean**| Should settings that are stored encrypted (type &#39;secretString&#39;) be included. Note: if included, the decrypted value will be returned.  | [optional] [default to false]
 **process_replacement_tokens** | **Boolean**| Whether to process replacement tokens (false returns the raw value of each setting, without tokens or environment variable replacements) | [optional] [default to true]

### Return type

[**SettingListSchema**](SettingListSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_course_version_info

> CourseSchema get_course_version_info(engine_tenant_name, course_id, version_id, opts)

Get version `versionId` of `courseId`

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
course_id = 'course_id_example' # String | 
version_id = 56 # Integer | the course version
opts = {
  include_registration_count: false, # Boolean | Include the registration count in the results
  include_course_metadata: false # Boolean | Include course metadata in the results
}

begin
  #Get version `versionId` of `courseId`
  result = api_instance.get_course_version_info(engine_tenant_name, course_id, version_id, opts)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->get_course_version_info: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **course_id** | **String**|  | 
 **version_id** | **Integer**| the course version | 
 **include_registration_count** | **Boolean**| Include the registration count in the results | [optional] [default to false]
 **include_course_metadata** | **Boolean**| Include course metadata in the results | [optional] [default to false]

### Return type

[**CourseSchema**](CourseSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_course_version_statements

> XapiStatementResult get_course_version_statements(engine_tenant_name, course_id, version_id, opts)

Get xAPI statements for version `versionId` of `courseId`

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
course_id = 'course_id_example' # String | 
version_id = 56 # Integer | the course version
opts = {
  learner_id: 'learner_id_example', # String | Only entries for the specified learner id will be included.
  since: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Only items updated since the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  _until: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Only items updated up until the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  more: 'more_example' # String | Value for this parameter will be provided in the 'more' property of lists, where needed. An opaque value, construction and parsing may change without notice.
}

begin
  #Get xAPI statements for version `versionId` of `courseId`
  result = api_instance.get_course_version_statements(engine_tenant_name, course_id, version_id, opts)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->get_course_version_statements: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **course_id** | **String**|  | 
 **version_id** | **Integer**| the course version | 
 **learner_id** | **String**| Only entries for the specified learner id will be included. | [optional] 
 **since** | **DateTime**| Only items updated since the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used. | [optional] 
 **_until** | **DateTime**| Only items updated up until the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used. | [optional] 
 **more** | **String**| Value for this parameter will be provided in the &#39;more&#39; property of lists, where needed. An opaque value, construction and parsing may change without notice. | [optional] 

### Return type

[**XapiStatementResult**](XapiStatementResult.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_course_versions

> CourseListNonPagedSchema get_course_versions(engine_tenant_name, course_id, opts)

Get all versions of `courseId`

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
course_id = 'course_id_example' # String | 
opts = {
  since: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Only items updated since the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  _until: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Only items updated up until the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  include_registration_count: false, # Boolean | Include the registration count in the results
  include_course_metadata: false # Boolean | Include course metadata in the results
}

begin
  #Get all versions of `courseId`
  result = api_instance.get_course_versions(engine_tenant_name, course_id, opts)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->get_course_versions: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **course_id** | **String**|  | 
 **since** | **DateTime**| Only items updated since the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used. | [optional] 
 **_until** | **DateTime**| Only items updated up until the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used. | [optional] 
 **include_registration_count** | **Boolean**| Include the registration count in the results | [optional] [default to false]
 **include_course_metadata** | **Boolean**| Include course metadata in the results | [optional] [default to false]

### Return type

[**CourseListNonPagedSchema**](CourseListNonPagedSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_courses

> CourseListSchema get_courses(engine_tenant_name, opts)

Get a list of all courses for the specified tenant

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
opts = {
  more: 'more_example', # String | Value for this parameter will be provided in the 'more' property of lists, where needed. An opaque value, construction and parsing may change without notice.
  since: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Only items updated since the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  _until: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Only items updated up until the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  include_registration_count: false, # Boolean | Include the registration count in the results
  include_course_metadata: false # Boolean | Include course metadata in the results
}

begin
  #Get a list of all courses for the specified tenant
  result = api_instance.get_courses(engine_tenant_name, opts)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->get_courses: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **more** | **String**| Value for this parameter will be provided in the &#39;more&#39; property of lists, where needed. An opaque value, construction and parsing may change without notice. | [optional] 
 **since** | **DateTime**| Only items updated since the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used. | [optional] 
 **_until** | **DateTime**| Only items updated up until the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used. | [optional] 
 **include_registration_count** | **Boolean**| Include the registration count in the results | [optional] [default to false]
 **include_course_metadata** | **Boolean**| Include course metadata in the results | [optional] [default to false]

### Return type

[**CourseListSchema**](CourseListSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_import_job_status

> ImportJobResultSchema get_import_job_status(engine_tenant_name, import_job_id)

Check the status of an import job.

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
import_job_id = 'import_job_id_example' # String | Id received when the import job was submitted to the importJobs resource.

begin
  #Check the status of an import job.
  result = api_instance.get_import_job_status(engine_tenant_name, import_job_id)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->get_import_job_status: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **import_job_id** | **String**| Id received when the import job was submitted to the importJobs resource. | 

### Return type

[**ImportJobResultSchema**](ImportJobResultSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## import_course_without_upload

> ImportResultSchema import_course_without_upload(engine_tenant_name, course_id, import_request, opts)

Create a course

Import the specified course and return the results of the import. For large imports, it may be necessary to use importJobs instead to avoid timeouts.

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
course_id = 'course_id_example' # String | A unique identifier your application will use to identify the course after import. Your application is responsible both for generating this unique ID and for keeping track of the ID for later use.
import_request = RusticiSoftwareCloudV2::ImportRequestSchema.new # ImportRequestSchema | 
opts = {
  may_create_new_version: false, # Boolean | Is it OK to create a new version of this course? If this is set to false and the course already exists, the upload will fail. If true and the course already exists then a new version will be created. No effect if the course doesn't already exist.
  dry_run: false # Boolean | Validate the course can be imported (mainly by validating the manifest), but don't actually import it.
}

begin
  #Create a course
  result = api_instance.import_course_without_upload(engine_tenant_name, course_id, import_request, opts)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->import_course_without_upload: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **course_id** | **String**| A unique identifier your application will use to identify the course after import. Your application is responsible both for generating this unique ID and for keeping track of the ID for later use. | 
 **import_request** | [**ImportRequestSchema**](ImportRequestSchema.md)|  | 
 **may_create_new_version** | **Boolean**| Is it OK to create a new version of this course? If this is set to false and the course already exists, the upload will fail. If true and the course already exists then a new version will be created. No effect if the course doesn&#39;t already exist. | [optional] [default to false]
 **dry_run** | **Boolean**| Validate the course can be imported (mainly by validating the manifest), but don&#39;t actually import it. | [optional] [default to false]

### Return type

[**ImportResultSchema**](ImportResultSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## set_course_configuration

> set_course_configuration(engine_tenant_name, course_id, configuration_settings)

Set configuration settings for this course.

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
course_id = 'course_id_example' # String | 
configuration_settings = RusticiSoftwareCloudV2::SettingsPostSchema.new # SettingsPostSchema | 

begin
  #Set configuration settings for this course.
  api_instance.set_course_configuration(engine_tenant_name, course_id, configuration_settings)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->set_course_configuration: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **course_id** | **String**|  | 
 **configuration_settings** | [**SettingsPostSchema**](SettingsPostSchema.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## set_course_title

> set_course_title(engine_tenant_name, course_id, title)

Sets the course title for `courseId`

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
course_id = 'course_id_example' # String | 
title = RusticiSoftwareCloudV2::TitleSchema.new # TitleSchema | 

begin
  #Sets the course title for `courseId`
  api_instance.set_course_title(engine_tenant_name, course_id, title)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->set_course_title: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **course_id** | **String**|  | 
 **title** | [**TitleSchema**](TitleSchema.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## set_course_version_configuration

> set_course_version_configuration(engine_tenant_name, course_id, version_id, configuration_settings)

Set configuration settings for this course and version.

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
course_id = 'course_id_example' # String | 
version_id = 56 # Integer | the course version
configuration_settings = RusticiSoftwareCloudV2::SettingsPostSchema.new # SettingsPostSchema | 

begin
  #Set configuration settings for this course and version.
  api_instance.set_course_version_configuration(engine_tenant_name, course_id, version_id, configuration_settings)
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->set_course_version_configuration: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **course_id** | **String**|  | 
 **version_id** | **Integer**| the course version | 
 **configuration_settings** | [**SettingsPostSchema**](SettingsPostSchema.md)|  | 

### Return type

nil (empty response body)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## upload_and_import_course

> ImportResultSchema upload_and_import_course(engine_tenant_name, course_id, opts)

Upload a course to import

Upload and import the specified course and return the results of the import. For large imports, it may be necessary to use importJobs instead to avoid timeouts.

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::CourseApi.new
engine_tenant_name = 'engine_tenant_name_example' # String | tenant for this request
course_id = 'course_id_example' # String | A unique identifier your application will use to identify the course after import. Your application is responsible both for generating this unique ID and for keeping track of the ID for later use.
opts = {
  may_create_new_version: false, # Boolean | Is it OK to create a new version of this course? If this is set to false and the course already exists, the upload will fail. If true and the course already exists then a new version will be created. No effect if the course doesn't already exist.
  dry_run: false, # Boolean | Validate the course can be imported (mainly by validating the manifest), but don't actually import it.
  file: File.new('/path/to/file') # File | The zip file of the course contents to import.
}

begin
  #Upload a course to import
  result = api_instance.upload_and_import_course(engine_tenant_name, course_id, opts)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling CourseApi->upload_and_import_course: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| tenant for this request | 
 **course_id** | **String**| A unique identifier your application will use to identify the course after import. Your application is responsible both for generating this unique ID and for keeping track of the ID for later use. | 
 **may_create_new_version** | **Boolean**| Is it OK to create a new version of this course? If this is set to false and the course already exists, the upload will fail. If true and the course already exists then a new version will be created. No effect if the course doesn&#39;t already exist. | [optional] [default to false]
 **dry_run** | **Boolean**| Validate the course can be imported (mainly by validating the manifest), but don&#39;t actually import it. | [optional] [default to false]
 **file** | **File**| The zip file of the course contents to import. | [optional] 

### Return type

[**ImportResultSchema**](ImportResultSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

