# RusticiSoftwareCloudV2::XapiAccount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**home_page** | **String** |  | 
**name** | **String** |  | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiAccount.new(home_page: null,
                                 name: null)
```


