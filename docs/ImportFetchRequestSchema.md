# RusticiSoftwareCloudV2::ImportFetchRequestSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** | URL path to the .zip package to fetch to import this course | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::ImportFetchRequestSchema.new(url: null)
```


