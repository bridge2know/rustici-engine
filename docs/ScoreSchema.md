# RusticiSoftwareCloudV2::ScoreSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scaled** | **Float** | Scaled score between 0 and 100 | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::ScoreSchema.new(scaled: null)
```


