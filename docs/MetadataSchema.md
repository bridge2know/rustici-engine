# RusticiSoftwareCloudV2::MetadataSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** |  | [optional] 
**title_language** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**description_language** | **String** |  | [optional] 
**duration** | **String** |  | [optional] 
**typical_time** | **String** |  | [optional] 
**keywords** | **Array&lt;String&gt;** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::MetadataSchema.new(title: null,
                                 title_language: null,
                                 description: null,
                                 description_language: null,
                                 duration: null,
                                 typical_time: null,
                                 keywords: null)
```


