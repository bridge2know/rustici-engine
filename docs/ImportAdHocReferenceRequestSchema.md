# RusticiSoftwareCloudV2::ImportAdHocReferenceRequestSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**learning_standard** | **String** | The learning standard for the course to import. | 
**title** | **String** | The title of the course to import. | 
**launch_url** | **String** | URL to the specific content file to launch when this course is launched. | 
**xapi_activity_id** | **String** | The xApi activity ID associated with this course. In general, this can be omitted if this course is not &#39;using xAPI and following the Tin Can launch guidelines&#39;. | [optional] 
**cmi5_publisher_id** | **String** | The publisher ID for a CMI5 course. This can be omitted if the learning standard is not CMI5. | [optional] 
**web_path_to_course** | **String** | This is the URL to the root of the course, where the course content is already available. | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::ImportAdHocReferenceRequestSchema.new(learning_standard: null,
                                 title: null,
                                 launch_url: http://example.org/content/course123/index.html,
                                 xapi_activity_id: null,
                                 cmi5_publisher_id: null,
                                 web_path_to_course: null)
```


