# RusticiSoftwareCloudV2::PIIDeletionResultSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**job_id** | **String** |  | [optional] 
**message** | **String** |  | [optional] 
**job_status** | **String** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::PIIDeletionResultSchema.new(job_id: null,
                                 message: null,
                                 job_status: null)
```


