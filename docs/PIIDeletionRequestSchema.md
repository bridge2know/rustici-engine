# RusticiSoftwareCloudV2::PIIDeletionRequestSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**learner_ids** | **Array&lt;String&gt;** |  | [optional] 
**agents** | **Array&lt;Object&gt;** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::PIIDeletionRequestSchema.new(learner_ids: null,
                                 agents: null)
```


