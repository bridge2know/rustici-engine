# RusticiSoftwareCloudV2::PermissionsSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scopes** | **Array&lt;String&gt;** |  | [optional] 
**tenant_name** | **String** | tenant these permissions apply to | [optional] 
**course_id** | **String** | course these permissions apply to | [optional] 
**registration_id** | **String** | registration these permissions apply to | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::PermissionsSchema.new(scopes: null,
                                 tenant_name: null,
                                 course_id: null,
                                 registration_id: null)
```


