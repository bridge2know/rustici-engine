# RusticiSoftwareCloudV2::RuntimeInteractionSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**objectives** | **Array&lt;String&gt;** |  | [optional] 
**timestamp** | **String** |  | [optional] 
**timestamp_utc** | **String** |  | [optional] 
**correct_responses** | **Array&lt;String&gt;** |  | [optional] 
**weighting** | **String** |  | [optional] 
**learner_response** | **String** |  | [optional] 
**result** | **String** |  | [optional] 
**latency** | **String** |  | [optional] 
**description** | **String** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::RuntimeInteractionSchema.new(id: null,
                                 type: null,
                                 objectives: null,
                                 timestamp: null,
                                 timestamp_utc: null,
                                 correct_responses: null,
                                 weighting: null,
                                 learner_response: null,
                                 result: null,
                                 latency: null,
                                 description: null)
```


