# RusticiSoftwareCloudV2::ObjectiveSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**primary** | **Boolean** |  | [optional] 
**score** | [**ScoreSchema**](ScoreSchema.md) |  | [optional] 
**score_max** | **Float** |  | [optional] 
**score_min** | **Float** |  | [optional] 
**score_raw** | **Float** |  | [optional] 
**previous_score_scaled** | **Float** |  | [optional] 
**first_score_scaled** | **Float** |  | [optional] 
**progress_measure** | **Float** |  | [optional] 
**first_success_time_stamp** | **String** |  | [optional] 
**objective_completion** | **String** |  | [optional] [default to &#39;UNKNOWN&#39;]
**objective_success** | **String** |  | [optional] [default to &#39;UNKNOWN&#39;]
**previous_objective_success** | **String** |  | [optional] [default to &#39;UNKNOWN&#39;]

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::ObjectiveSchema.new(id: null,
                                 primary: null,
                                 score: null,
                                 score_max: null,
                                 score_min: null,
                                 score_raw: null,
                                 previous_score_scaled: null,
                                 first_score_scaled: null,
                                 progress_measure: null,
                                 first_success_time_stamp: null,
                                 objective_completion: null,
                                 objective_success: null,
                                 previous_objective_success: null)
```


