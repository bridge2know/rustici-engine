# RusticiSoftwareCloudV2::CourseReferenceSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**title** | **String** |  | [optional] 
**version** | **Integer** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::CourseReferenceSchema.new(id: null,
                                 title: null,
                                 version: null)
```


