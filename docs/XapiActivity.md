# RusticiSoftwareCloudV2::XapiActivity

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**object_type** | **String** |  | [optional] [default to &#39;Activity&#39;]
**id** | **String** |  | 
**definition** | [**XapiActivityDefinition**](XapiActivityDefinition.md) |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiActivity.new(object_type: null,
                                 id: null,
                                 definition: null)
```


