# RusticiSoftwareCloudV2::DispatchIdSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**data** | [**DispatchSchema**](DispatchSchema.md) |  | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::DispatchIdSchema.new(id: null,
                                 data: null)
```


