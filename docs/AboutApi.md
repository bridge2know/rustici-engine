# RusticiSoftwareCloudV2::AboutApi

All URIs are relative to *http://http:/api/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_about**](AboutApi.md#get_about) | **GET** /about | Get back the version and platform of the running instance of Engine
[**get_user_count**](AboutApi.md#get_user_count) | **GET** /about/userCount | Gets the number of users for the specified tenant or across all tenants when none is specified



## get_about

> AboutSchema get_about(opts)

Get back the version and platform of the running instance of Engine

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::AboutApi.new
opts = {
  engine_tenant_name: 'engine_tenant_name_example' # String | optional tenant for this request
}

begin
  #Get back the version and platform of the running instance of Engine
  result = api_instance.get_about(opts)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling AboutApi->get_about: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| optional tenant for this request | [optional] 

### Return type

[**AboutSchema**](AboutSchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_user_count

> UserCountSummarySchema get_user_count(opts)

Gets the number of users for the specified tenant or across all tenants when none is specified

### Example

```ruby
# load the gem
require 'rustici_software_cloud_v2'
# setup authorization
RusticiSoftwareCloudV2.configure do |config|
  # Configure HTTP basic authorization: basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'

  # Configure OAuth2 access token for authorization: oauth
  config.access_token = 'YOUR ACCESS TOKEN'
end

api_instance = RusticiSoftwareCloudV2::AboutApi.new
opts = {
  engine_tenant_name: 'engine_tenant_name_example', # String | optional tenant for this request
  since: DateTime.parse('2013-10-20T19:20:30+01:00'), # DateTime | Only items updated since the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  _until: DateTime.parse('2013-10-20T19:20:30+01:00') # DateTime | Only items updated up until the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
}

begin
  #Gets the number of users for the specified tenant or across all tenants when none is specified
  result = api_instance.get_user_count(opts)
  p result
rescue RusticiSoftwareCloudV2::ApiError => e
  puts "Exception when calling AboutApi->get_user_count: #{e}"
end
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **engine_tenant_name** | **String**| optional tenant for this request | [optional] 
 **since** | **DateTime**| Only items updated since the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used. | [optional] 
 **_until** | **DateTime**| Only items updated up until the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used. | [optional] 

### Return type

[**UserCountSummarySchema**](UserCountSummarySchema.md)

### Authorization

[basic](../README.md#basic), [oauth](../README.md#oauth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

