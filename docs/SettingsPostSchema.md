# RusticiSoftwareCloudV2::SettingsPostSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**settings** | [**Array&lt;SettingsIndividualSchema&gt;**](SettingsIndividualSchema.md) |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::SettingsPostSchema.new(settings: null)
```


