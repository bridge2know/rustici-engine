# RusticiSoftwareCloudV2::LtiReporterIdSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lti_reporter_id** | **String** | The ID of the LTI reporter that was just created. | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::LtiReporterIdSchema.new(lti_reporter_id: null)
```


