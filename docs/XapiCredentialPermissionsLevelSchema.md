# RusticiSoftwareCloudV2::XapiCredentialPermissionsLevelSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**xapi_credential_permissions_level** | **String** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiCredentialPermissionsLevelSchema.new(xapi_credential_permissions_level: null)
```


