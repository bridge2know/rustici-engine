# RusticiSoftwareCloudV2::ItemValuePairSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**item** | **String** |  | [optional] 
**value** | **String** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::ItemValuePairSchema.new(item: null,
                                 value: null)
```


