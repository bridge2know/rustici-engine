# RusticiSoftwareCloudV2::XapiCredentialPostSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**info** | **String** |  | [optional] 
**secret** | **String** |  | [optional] 
**is_enabled** | **Boolean** |  | 
**auth** | [**XapiCredentialAuthTypeSchema**](XapiCredentialAuthTypeSchema.md) |  | 
**permissions_level** | [**XapiCredentialPermissionsLevelSchema**](XapiCredentialPermissionsLevelSchema.md) |  | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiCredentialPostSchema.new(name: null,
                                 info: null,
                                 secret: null,
                                 is_enabled: null,
                                 auth: null,
                                 permissions_level: null)
```


