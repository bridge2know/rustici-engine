# RusticiSoftwareCloudV2::XapiStatementPipeSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**last_forwarded_statement_date** | **String** |  | [optional] 
**more_url** | **String** |  | [optional] 
**attempts** | **Integer** |  | [optional] 
**visible_after** | **String** |  | [optional] 
**source** | [**XapiEndpointSchema**](XapiEndpointSchema.md) |  | [optional] 
**target** | [**XapiEndpointSchema**](XapiEndpointSchema.md) |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiStatementPipeSchema.new(id: null,
                                 last_forwarded_statement_date: null,
                                 more_url: null,
                                 attempts: null,
                                 visible_after: null,
                                 source: null,
                                 target: null)
```


