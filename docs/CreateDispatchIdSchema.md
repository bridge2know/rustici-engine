# RusticiSoftwareCloudV2::CreateDispatchIdSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**data** | [**CreateDispatchSchema**](CreateDispatchSchema.md) |  | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::CreateDispatchIdSchema.new(id: null,
                                 data: null)
```


