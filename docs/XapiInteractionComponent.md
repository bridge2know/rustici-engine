# RusticiSoftwareCloudV2::XapiInteractionComponent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**description** | **Hash&lt;String, String&gt;** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiInteractionComponent.new(id: null,
                                 description: null)
```


