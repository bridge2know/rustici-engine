# RusticiSoftwareCloudV2::XapiCredentialsListSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**xapi_credentials** | [**Array&lt;XapiCredentialSchema&gt;**](XapiCredentialSchema.md) |  | 
**more** | **String** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiCredentialsListSchema.new(xapi_credentials: null,
                                 more: null)
```


