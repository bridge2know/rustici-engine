# RusticiSoftwareCloudV2::RuntimeSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**completion_status** | **String** |  | [optional] 
**credit** | **String** |  | [optional] 
**entry** | **String** |  | [optional] 
**exit** | **String** |  | [optional] 
**learner_preference** | [**LearnerPreferenceSchema**](LearnerPreferenceSchema.md) |  | [optional] 
**location** | **String** |  | [optional] 
**mode** | **String** |  | [optional] 
**progress_measure** | **String** |  | [optional] 
**score_scaled** | **String** |  | [optional] 
**score_raw** | **String** |  | [optional] 
**score_min** | **String** |  | [optional] 
**score_max** | **String** |  | [optional] 
**total_time** | **String** |  | [optional] 
**time_tracked** | **String** |  | [optional] 
**runtime_success_status** | **String** |  | [optional] [default to &#39;UNKNOWN&#39;]
**suspend_data** | **String** |  | [optional] 
**learner_comments** | [**Array&lt;CommentSchema&gt;**](CommentSchema.md) |  | [optional] 
**lms_comments** | [**Array&lt;CommentSchema&gt;**](CommentSchema.md) |  | [optional] 
**runtime_interactions** | [**Array&lt;RuntimeInteractionSchema&gt;**](RuntimeInteractionSchema.md) |  | [optional] 
**runtime_objectives** | [**Array&lt;RuntimeObjectiveSchema&gt;**](RuntimeObjectiveSchema.md) |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::RuntimeSchema.new(completion_status: null,
                                 credit: null,
                                 entry: null,
                                 exit: null,
                                 learner_preference: null,
                                 location: null,
                                 mode: null,
                                 progress_measure: null,
                                 score_scaled: null,
                                 score_raw: null,
                                 score_min: null,
                                 score_max: null,
                                 total_time: null,
                                 time_tracked: null,
                                 runtime_success_status: null,
                                 suspend_data: null,
                                 learner_comments: null,
                                 lms_comments: null,
                                 runtime_interactions: null,
                                 runtime_objectives: null)
```


