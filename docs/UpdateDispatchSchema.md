# RusticiSoftwareCloudV2::UpdateDispatchSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allow_new_registrations** | **Boolean** | If true, then new registrations can be created for this dispatch. | [optional] 
**instanced** | **Boolean** | If true, then a new registration instance will be created if the client LMS doesn&#39;t provide launch data for an existing one. Otherwise, the same instance will always be used for the given cmi.learner_id. | [optional] 
**registration_cap** | **Integer** | The maximum number of registrations that can be created for this dispatch, where &#39;0&#39; means &#39;unlimited registrations&#39;. | [optional] 
**expiration_date** | **DateTime** | The date after which this dispatch will be disabled as an ISO 8601 string. | [optional] 
**enabled** | **Boolean** | If true, then this dispatch can be launched. | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::UpdateDispatchSchema.new(allow_new_registrations: null,
                                 instanced: null,
                                 registration_cap: null,
                                 expiration_date: null,
                                 enabled: null)
```


