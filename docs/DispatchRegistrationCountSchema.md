# RusticiSoftwareCloudV2::DispatchRegistrationCountSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**registration_count** | **Integer** |  | [optional] 
**last_reset_date** | **DateTime** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::DispatchRegistrationCountSchema.new(registration_count: null,
                                 last_reset_date: null)
```


