# RusticiSoftwareCloudV2::CredentialRequestSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | name for this credential | [optional] 
**permissions** | [**PermissionsSchema**](PermissionsSchema.md) |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::CredentialRequestSchema.new(name: null,
                                 permissions: null)
```


