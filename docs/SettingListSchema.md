# RusticiSoftwareCloudV2::SettingListSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**setting_items** | [**Array&lt;SettingItem&gt;**](SettingItem.md) |  | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::SettingListSchema.new(setting_items: null)
```


