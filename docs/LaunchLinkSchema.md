# RusticiSoftwareCloudV2::LaunchLinkSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**launch_link** | **String** |  | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::LaunchLinkSchema.new(launch_link: null)
```


