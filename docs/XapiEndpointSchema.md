# RusticiSoftwareCloudV2::XapiEndpointSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  | [optional] 
**user_name** | **String** |  | [optional] 
**password** | **String** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiEndpointSchema.new(url: null,
                                 user_name: null,
                                 password: null)
```


