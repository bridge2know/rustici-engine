# RusticiSoftwareCloudV2::AboutSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version** | **String** |  | 
**platform** | **String** |  | 
**distribution** | **String** |  | [optional] 
**integration** | **String** | Integration class. Only relevant to some customers with Engine installations from before the 2015 release. | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::AboutSchema.new(version: null,
                                 platform: null,
                                 distribution: null,
                                 integration: null)
```


