# RusticiSoftwareCloudV2::CreateDispatchSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**destination_id** | **String** | Destination Id | 
**course_id** | **String** | Serialized external package ID. | 
**allow_new_registrations** | **Boolean** | If true, then new registrations can be created for this dispatch. | [optional] [default to true]
**instanced** | **Boolean** | If true, then a new registration instance will be created if the client LMS doesn&#39;t provide launch data for an existing one. Otherwise, the same instance will always be used for the given cmi.learner_id. | [optional] [default to true]
**registration_cap** | **Integer** | The maximum number of registrations that can be created for this dispatch, where &#39;0&#39; means &#39;unlimited registrations&#39;. | [optional] [default to 0]
**expiration_date** | **DateTime** | The date after which this dispatch will be disabled as an ISO 8601 string, or not present for no expiration date. | [optional] 
**enabled** | **Boolean** | If true, then this dispatch can be launched. | [optional] [default to true]

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::CreateDispatchSchema.new(destination_id: null,
                                 course_id: null,
                                 allow_new_registrations: null,
                                 instanced: null,
                                 registration_cap: null,
                                 expiration_date: null,
                                 enabled: null)
```


