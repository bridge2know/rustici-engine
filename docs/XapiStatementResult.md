# RusticiSoftwareCloudV2::XapiStatementResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**statements** | [**Array&lt;XapiStatement&gt;**](XapiStatement.md) |  | 
**more** | **String** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiStatementResult.new(statements: null,
                                 more: null)
```


