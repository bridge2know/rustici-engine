# RusticiSoftwareCloudV2::LaunchLinkRequestSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expiry** | **Integer** | The number of seconds from now that this link will expire in. This parameter should only be specified if the setting &#39;ApiUseSignedLaunchLinks&#39; is configured with a value of &#39;true&#39;. | [optional] [default to 120]
**redirect_on_exit_url** | **String** | The URL the application should redirect to when the learner exits a course. If not specified, configured value will be used. | [optional] 
**tracking** | **Boolean** | Should this launch be tracked? If false, Engine will avoid tracking to the extent possible for the standard being used. | [optional] [default to true]
**start_sco** | **String** | For SCORM, SCO identifier to override launch, overriding the normal sequencing. | [optional] 
**additional_values** | [**Array&lt;ItemValuePairSchema&gt;**](ItemValuePairSchema.md) |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::LaunchLinkRequestSchema.new(expiry: null,
                                 redirect_on_exit_url: null,
                                 tracking: null,
                                 start_sco: null,
                                 additional_values: null)
```


