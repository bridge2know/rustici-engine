# RusticiSoftwareCloudV2::LearnerPreferenceSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**audio_level** | **Float** |  | [optional] 
**language** | **String** |  | [optional] 
**delivery_speed** | **Float** |  | [optional] 
**audio_captioning** | **Integer** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::LearnerPreferenceSchema.new(audio_level: null,
                                 language: null,
                                 delivery_speed: null,
                                 audio_captioning: null)
```


