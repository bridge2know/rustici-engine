# RusticiSoftwareCloudV2::SettingValidValue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value_description** | **String** | Description of what this valid value means, how it will be applied if used in a setting | [optional] 
**value** | **String** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::SettingValidValue.new(value_description: null,
                                 value: null)
```


