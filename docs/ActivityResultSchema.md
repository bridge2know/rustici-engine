# RusticiSoftwareCloudV2::ActivityResultSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**title** | **String** |  | [optional] 
**attempts** | **Integer** |  | [optional] 
**activity_completion** | **String** |  | [optional] 
**activity_success** | **String** |  | [optional] 
**score** | [**ScoreSchema**](ScoreSchema.md) |  | [optional] 
**time_tracked** | **String** |  | [optional] 
**completion_amount** | [**CompletionAmountSchema**](CompletionAmountSchema.md) |  | [optional] 
**suspended** | **Boolean** |  | [optional] 
**children** | [**Array&lt;ActivityResultSchema&gt;**](ActivityResultSchema.md) |  | [optional] 
**objectives** | [**Array&lt;ObjectiveSchema&gt;**](ObjectiveSchema.md) |  | [optional] 
**static_properties** | [**StaticPropertiesSchema**](StaticPropertiesSchema.md) |  | [optional] 
**runtime** | [**RuntimeSchema**](RuntimeSchema.md) |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::ActivityResultSchema.new(id: null,
                                 title: null,
                                 attempts: null,
                                 activity_completion: null,
                                 activity_success: null,
                                 score: null,
                                 time_tracked: null,
                                 completion_amount: null,
                                 suspended: null,
                                 children: null,
                                 objectives: null,
                                 static_properties: null,
                                 runtime: null)
```


