# RusticiSoftwareCloudV2::XapiAgentGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**object_type** | **String** |  | 
**name** | **String** |  | [optional] 
**mbox** | **String** |  | [optional] 
**mbox_sha1sum** | **String** |  | [optional] 
**openid** | **String** |  | [optional] 
**account** | [**XapiAccount**](XapiAccount.md) |  | [optional] 
**member** | [**Array&lt;XapiAgentGroup&gt;**](XapiAgentGroup.md) |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiAgentGroup.new(object_type: null,
                                 name: null,
                                 mbox: null,
                                 mbox_sha1sum: null,
                                 openid: null,
                                 account: null,
                                 member: null)
```


