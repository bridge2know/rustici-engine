# RusticiSoftwareCloudV2::UserCountSummarySchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**combined_tenants** | [**UserCountDetailSchema**](UserCountDetailSchema.md) |  | 
**by_tenant** | [**Array&lt;UserCountDetailSchema&gt;**](UserCountDetailSchema.md) |  | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::UserCountSummarySchema.new(combined_tenants: null,
                                 by_tenant: null)
```


