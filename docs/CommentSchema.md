# RusticiSoftwareCloudV2::CommentSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **String** |  | [optional] 
**location** | **String** |  | [optional] 
**date_time** | **String** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::CommentSchema.new(value: null,
                                 location: null,
                                 date_time: null)
```


