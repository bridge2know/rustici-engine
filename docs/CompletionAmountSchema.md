# RusticiSoftwareCloudV2::CompletionAmountSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scaled** | **Float** | Scaled completion amount between 0 and 100 | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::CompletionAmountSchema.new(scaled: null)
```


