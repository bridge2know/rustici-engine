# RusticiSoftwareCloudV2::XapiResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**score** | [**XapiScore**](XapiScore.md) |  | [optional] 
**success** | **Boolean** |  | [optional] 
**completion** | **Boolean** |  | [optional] 
**response** | **String** |  | [optional] 
**duration** | **String** |  | [optional] 
**extensions** | **Hash&lt;String, Object&gt;** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiResult.new(score: null,
                                 success: null,
                                 completion: null,
                                 response: null,
                                 duration: null,
                                 extensions: null)
```


