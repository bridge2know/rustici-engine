# RusticiSoftwareCloudV2::RegistrationInstancingSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **Boolean** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::RegistrationInstancingSchema.new(enabled: null)
```


