# RusticiSoftwareCloudV2::StringResultSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | **String** |  | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::StringResultSchema.new(result: null)
```


