# RusticiSoftwareCloudV2::XapiVerb

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**display** | **Hash&lt;String, String&gt;** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiVerb.new(id: null,
                                 display: null)
```


