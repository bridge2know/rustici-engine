# RusticiSoftwareCloudV2::SettingsIndividualSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**setting_id** | **String** |  | [optional] 
**value** | **String** |  | [optional] 
**explicit** | **Boolean** |  | [optional] [default to false]

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::SettingsIndividualSchema.new(setting_id: null,
                                 value: null,
                                 explicit: null)
```


