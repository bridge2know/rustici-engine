# RusticiSoftwareCloudV2::LaunchPageResponseSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**launch_url** | **String** | The Engine launch URL for the dispatch registration, or null if there was an error. | 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::LaunchPageResponseSchema.new(launch_url: null)
```


