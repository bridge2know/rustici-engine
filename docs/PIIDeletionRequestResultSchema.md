# RusticiSoftwareCloudV2::PIIDeletionRequestResultSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**job_id** | **String** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::PIIDeletionRequestResultSchema.new(job_id: null)
```


