# RusticiSoftwareCloudV2::ImportRequestSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fetch_request** | [**ImportFetchRequestSchema**](ImportFetchRequestSchema.md) |  | [optional] 
**reference_request** | [**ImportReferenceRequestSchema**](ImportReferenceRequestSchema.md) |  | [optional] 
**ad_hoc_reference_request** | [**ImportAdHocReferenceRequestSchema**](ImportAdHocReferenceRequestSchema.md) |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::ImportRequestSchema.new(fetch_request: null,
                                 reference_request: null,
                                 ad_hoc_reference_request: null)
```


