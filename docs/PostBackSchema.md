# RusticiSoftwareCloudV2::PostBackSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **String** |  | [optional] 
**auth_type** | **String** |  | [optional] [default to &#39;UNDEFINED&#39;]
**user_name** | **String** | The user name to be used in authorizing the postback of data to the URL specified by postback url. | [optional] 
**password** | **String** | The password to be used in authorizing the postback of data to the URL specified by postback url. | [optional] 
**results_format** | **String** |  | [optional] [default to &#39;UNDEFINED&#39;]

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::PostBackSchema.new(url: null,
                                 auth_type: null,
                                 user_name: null,
                                 password: null,
                                 results_format: null)
```


