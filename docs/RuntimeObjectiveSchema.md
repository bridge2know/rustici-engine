# RusticiSoftwareCloudV2::RuntimeObjectiveSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**score_scaled** | **String** |  | [optional] 
**score_min** | **String** |  | [optional] 
**score_max** | **String** |  | [optional] 
**score_raw** | **String** |  | [optional] 
**runtime_objective_success_status** | **String** |  | [optional] [default to &#39;UNKNOWN&#39;]
**runtime_objective_completion_status** | **String** |  | [optional] [default to &#39;UNKNOWN&#39;]
**progress_measure** | **String** |  | [optional] 
**description** | **String** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::RuntimeObjectiveSchema.new(id: null,
                                 score_scaled: null,
                                 score_min: null,
                                 score_max: null,
                                 score_raw: null,
                                 runtime_objective_success_status: null,
                                 runtime_objective_completion_status: null,
                                 progress_measure: null,
                                 description: null)
```


