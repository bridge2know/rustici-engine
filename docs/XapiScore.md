# RusticiSoftwareCloudV2::XapiScore

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scaled** | **Float** |  | [optional] 
**raw** | **Float** |  | [optional] 
**min** | **Float** |  | [optional] 
**max** | **Float** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::XapiScore.new(scaled: null,
                                 raw: null,
                                 min: null,
                                 max: null)
```


