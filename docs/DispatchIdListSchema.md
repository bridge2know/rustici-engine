# RusticiSoftwareCloudV2::DispatchIdListSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ids** | **Array&lt;String&gt;** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::DispatchIdListSchema.new(ids: null)
```


