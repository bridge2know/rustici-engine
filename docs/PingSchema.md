# RusticiSoftwareCloudV2::PingSchema

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**api_message** | **String** |  | [optional] 
**current_time** | **String** |  | [optional] 
**database_message** | **String** |  | [optional] 

## Code Sample

```ruby
require 'RusticiSoftwareCloudV2'

instance = RusticiSoftwareCloudV2::PingSchema.new(api_message: null,
                                 current_time: null,
                                 database_message: null)
```


