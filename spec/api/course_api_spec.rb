=begin
#Rustici Engine API

#Rustici Engine API

OpenAPI spec version: 2.0

Generated by: https://openapi-generator.tech
OpenAPI Generator version: 4.0.0-beta3

=end

require 'spec_helper'
require 'json'

# Unit tests for RusticiSoftwareCloudV2::CourseApi
# Automatically generated by openapi-generator (https://openapi-generator.tech)
# Please update as you see appropriate
describe 'CourseApi' do
  before do
    # run before each test
    @api_instance = RusticiSoftwareCloudV2::CourseApi.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of CourseApi' do
    it 'should create an instance of CourseApi' do
      expect(@api_instance).to be_instance_of(RusticiSoftwareCloudV2::CourseApi)
    end
  end

  # unit tests for build_course_preview_launch_link
  # Returns the link to use to preview this course
  # @param engine_tenant_name tenant for this request
  # @param course_id 
  # @param launch_link_request 
  # @param [Hash] opts the optional parameters
  # @return [LaunchLinkSchema]
  describe 'build_course_preview_launch_link test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for build_course_preview_launch_link_with_version
  # Returns the link to use to preview this course
  # @param engine_tenant_name tenant for this request
  # @param course_id 
  # @param version_id the course version
  # @param launch_link_request 
  # @param [Hash] opts the optional parameters
  # @return [LaunchLinkSchema]
  describe 'build_course_preview_launch_link_with_version test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for create_fetch_and_import_course_job
  # Start a job to fetch and import a course.
  # An import job will be started to fetch and import the referenced file, and the import job ID will be returned. If the import is successful, the imported course will be registered using the courseId provided.
  # @param engine_tenant_name tenant for this request
  # @param course_id A unique identifier your application will use to identify the course after import. Your application is responsible both for generating this unique ID and for keeping track of the ID for later use.
  # @param import_request 
  # @param [Hash] opts the optional parameters
  # @option opts [Boolean] :may_create_new_version Is it OK to create a new version of this course? If this is set to false and the course already exists, the upload will fail. If true and the course already exists then a new version will be created. No effect if the course doesn&#39;t already exist.
  # @return [StringResultSchema]
  describe 'create_fetch_and_import_course_job test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for create_upload_and_import_course_job
  # Upload a course and start an import job for it.
  # An import job will be started to import the posted file, and the import job ID will be returned. If the import is successful, the imported course will be registered using the courseId provided.
  # @param engine_tenant_name tenant for this request
  # @param course_id A unique identifier your application will use to identify the course after import. Your application is responsible both for generating this unique ID and for keeping track of the ID for later use.
  # @param [Hash] opts the optional parameters
  # @option opts [Boolean] :may_create_new_version Is it OK to create a new version of this course? If this is set to false and the course already exists, the upload will fail. If true and the course already exists then a new version will be created. No effect if the course doesn&#39;t already exist.
  # @option opts [File] :file The zip file of the course contents to import.
  # @return [StringResultSchema]
  describe 'create_upload_and_import_course_job test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for delete_course
  # Delete &#x60;courseId&#x60;
  # @param engine_tenant_name tenant for this request
  # @param course_id 
  # @param [Hash] opts the optional parameters
  # @return [nil]
  describe 'delete_course test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for delete_course_configuration_setting
  # Clears the &#x60;settingId&#x60; value for this course
  # @param engine_tenant_name tenant for this request
  # @param course_id 
  # @param setting_id 
  # @param [Hash] opts the optional parameters
  # @return [nil]
  describe 'delete_course_configuration_setting test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for delete_course_version
  # Delete version &#x60;versionId&#x60; of &#x60;courseId&#x60;
  # @param engine_tenant_name tenant for this request
  # @param course_id 
  # @param version_id the course version
  # @param [Hash] opts the optional parameters
  # @return [nil]
  describe 'delete_course_version test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for delete_course_version_configuration_setting
  # Clears the &#x60;settingId&#x60; value for this course and version.
  # @param engine_tenant_name tenant for this request
  # @param course_id 
  # @param version_id the course version
  # @param setting_id 
  # @param [Hash] opts the optional parameters
  # @return [nil]
  describe 'delete_course_version_configuration_setting test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for get_course
  # Get information about &#x60;courseId&#x60;
  # @param engine_tenant_name tenant for this request
  # @param course_id 
  # @param [Hash] opts the optional parameters
  # @option opts [Boolean] :include_registration_count Include the registration count in the results
  # @option opts [Boolean] :include_course_metadata Include course metadata in the results
  # @return [CourseSchema]
  describe 'get_course test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for get_course_configuration
  # Returns all configuration settings for this course
  # @param engine_tenant_name tenant for this request
  # @param course_id 
  # @param [Hash] opts the optional parameters
  # @option opts [Boolean] :include_metadata 
  # @option opts [Boolean] :include_hidden_settings Should settings that are declared to be hidden be included. Note: such settings generally do not need to be modified, and may be confusing. 
  # @option opts [Boolean] :include_secret_settings Should settings that are stored encrypted (type &#39;secretString&#39;) be included. Note: if included, the decrypted value will be returned. 
  # @option opts [Boolean] :process_replacement_tokens Whether to process replacement tokens (false returns the raw value of each setting, without tokens or environment variable replacements)
  # @return [SettingListSchema]
  describe 'get_course_configuration test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for get_course_statements
  # Get xAPI statements for &#x60;courseId&#x60;
  # @param engine_tenant_name tenant for this request
  # @param course_id 
  # @param [Hash] opts the optional parameters
  # @option opts [String] :learner_id Only entries for the specified learner id will be included.
  # @option opts [DateTime] :since Only items updated since the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  # @option opts [DateTime] :_until Only items updated up until the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  # @option opts [String] :more Value for this parameter will be provided in the &#39;more&#39; property of lists, where needed. An opaque value, construction and parsing may change without notice.
  # @return [XapiStatementResult]
  describe 'get_course_statements test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for get_course_version_configuration
  # Returns all configuration settings for this course and version.
  # @param engine_tenant_name tenant for this request
  # @param course_id 
  # @param version_id the course version
  # @param [Hash] opts the optional parameters
  # @option opts [Boolean] :include_metadata 
  # @option opts [Boolean] :include_hidden_settings Should settings that are declared to be hidden be included. Note: such settings generally do not need to be modified, and may be confusing. 
  # @option opts [Boolean] :include_secret_settings Should settings that are stored encrypted (type &#39;secretString&#39;) be included. Note: if included, the decrypted value will be returned. 
  # @option opts [Boolean] :process_replacement_tokens Whether to process replacement tokens (false returns the raw value of each setting, without tokens or environment variable replacements)
  # @return [SettingListSchema]
  describe 'get_course_version_configuration test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for get_course_version_info
  # Get version &#x60;versionId&#x60; of &#x60;courseId&#x60;
  # @param engine_tenant_name tenant for this request
  # @param course_id 
  # @param version_id the course version
  # @param [Hash] opts the optional parameters
  # @option opts [Boolean] :include_registration_count Include the registration count in the results
  # @option opts [Boolean] :include_course_metadata Include course metadata in the results
  # @return [CourseSchema]
  describe 'get_course_version_info test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for get_course_version_statements
  # Get xAPI statements for version &#x60;versionId&#x60; of &#x60;courseId&#x60;
  # @param engine_tenant_name tenant for this request
  # @param course_id 
  # @param version_id the course version
  # @param [Hash] opts the optional parameters
  # @option opts [String] :learner_id Only entries for the specified learner id will be included.
  # @option opts [DateTime] :since Only items updated since the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  # @option opts [DateTime] :_until Only items updated up until the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  # @option opts [String] :more Value for this parameter will be provided in the &#39;more&#39; property of lists, where needed. An opaque value, construction and parsing may change without notice.
  # @return [XapiStatementResult]
  describe 'get_course_version_statements test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for get_course_versions
  # Get all versions of &#x60;courseId&#x60;
  # @param engine_tenant_name tenant for this request
  # @param course_id 
  # @param [Hash] opts the optional parameters
  # @option opts [DateTime] :since Only items updated since the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  # @option opts [DateTime] :_until Only items updated up until the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  # @option opts [Boolean] :include_registration_count Include the registration count in the results
  # @option opts [Boolean] :include_course_metadata Include course metadata in the results
  # @return [CourseListNonPagedSchema]
  describe 'get_course_versions test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for get_courses
  # Get a list of all courses for the specified tenant
  # @param engine_tenant_name tenant for this request
  # @param [Hash] opts the optional parameters
  # @option opts [String] :more Value for this parameter will be provided in the &#39;more&#39; property of lists, where needed. An opaque value, construction and parsing may change without notice.
  # @option opts [DateTime] :since Only items updated since the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  # @option opts [DateTime] :_until Only items updated up until the specified ISO 8601 TimeStamp (inclusive) are included. If a time zone is not specified, UTC time zone will be used.
  # @option opts [Boolean] :include_registration_count Include the registration count in the results
  # @option opts [Boolean] :include_course_metadata Include course metadata in the results
  # @return [CourseListSchema]
  describe 'get_courses test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for get_import_job_status
  # Check the status of an import job.
  # @param engine_tenant_name tenant for this request
  # @param import_job_id Id received when the import job was submitted to the importJobs resource.
  # @param [Hash] opts the optional parameters
  # @return [ImportJobResultSchema]
  describe 'get_import_job_status test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for import_course_without_upload
  # Create a course
  # Import the specified course and return the results of the import. For large imports, it may be necessary to use importJobs instead to avoid timeouts.
  # @param engine_tenant_name tenant for this request
  # @param course_id A unique identifier your application will use to identify the course after import. Your application is responsible both for generating this unique ID and for keeping track of the ID for later use.
  # @param import_request 
  # @param [Hash] opts the optional parameters
  # @option opts [Boolean] :may_create_new_version Is it OK to create a new version of this course? If this is set to false and the course already exists, the upload will fail. If true and the course already exists then a new version will be created. No effect if the course doesn&#39;t already exist.
  # @option opts [Boolean] :dry_run Validate the course can be imported (mainly by validating the manifest), but don&#39;t actually import it.
  # @return [ImportResultSchema]
  describe 'import_course_without_upload test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for set_course_configuration
  # Set configuration settings for this course.
  # @param engine_tenant_name tenant for this request
  # @param course_id 
  # @param configuration_settings 
  # @param [Hash] opts the optional parameters
  # @return [nil]
  describe 'set_course_configuration test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for set_course_title
  # Sets the course title for &#x60;courseId&#x60;
  # @param engine_tenant_name tenant for this request
  # @param course_id 
  # @param title 
  # @param [Hash] opts the optional parameters
  # @return [nil]
  describe 'set_course_title test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for set_course_version_configuration
  # Set configuration settings for this course and version.
  # @param engine_tenant_name tenant for this request
  # @param course_id 
  # @param version_id the course version
  # @param configuration_settings 
  # @param [Hash] opts the optional parameters
  # @return [nil]
  describe 'set_course_version_configuration test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for upload_and_import_course
  # Upload a course to import
  # Upload and import the specified course and return the results of the import. For large imports, it may be necessary to use importJobs instead to avoid timeouts.
  # @param engine_tenant_name tenant for this request
  # @param course_id A unique identifier your application will use to identify the course after import. Your application is responsible both for generating this unique ID and for keeping track of the ID for later use.
  # @param [Hash] opts the optional parameters
  # @option opts [Boolean] :may_create_new_version Is it OK to create a new version of this course? If this is set to false and the course already exists, the upload will fail. If true and the course already exists then a new version will be created. No effect if the course doesn&#39;t already exist.
  # @option opts [Boolean] :dry_run Validate the course can be imported (mainly by validating the manifest), but don&#39;t actually import it.
  # @option opts [File] :file The zip file of the course contents to import.
  # @return [ImportResultSchema]
  describe 'upload_and_import_course test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end
