=begin
#Rustici Engine API

#Rustici Engine API

OpenAPI spec version: 2.0

Generated by: https://openapi-generator.tech
OpenAPI Generator version: 4.0.0-beta3

=end

require 'spec_helper'
require 'json'
require 'date'

# Unit tests for RusticiSoftwareCloudV2::PIIDeletionRequestSchema
# Automatically generated by openapi-generator (https://openapi-generator.tech)
# Please update as you see appropriate
describe 'PIIDeletionRequestSchema' do
  before do
    # run before each test
    @instance = RusticiSoftwareCloudV2::PIIDeletionRequestSchema.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of PIIDeletionRequestSchema' do
    it 'should create an instance of PIIDeletionRequestSchema' do
      expect(@instance).to be_instance_of(RusticiSoftwareCloudV2::PIIDeletionRequestSchema)
    end
  end
  describe 'test attribute "learner_ids"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "agents"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end
