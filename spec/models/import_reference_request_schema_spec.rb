=begin
#Rustici Engine API

#Rustici Engine API

OpenAPI spec version: 2.0

Generated by: https://openapi-generator.tech
OpenAPI Generator version: 4.0.0-beta3

=end

require 'spec_helper'
require 'json'
require 'date'

# Unit tests for RusticiSoftwareCloudV2::ImportReferenceRequestSchema
# Automatically generated by openapi-generator (https://openapi-generator.tech)
# Please update as you see appropriate
describe 'ImportReferenceRequestSchema' do
  before do
    # run before each test
    @instance = RusticiSoftwareCloudV2::ImportReferenceRequestSchema.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of ImportReferenceRequestSchema' do
    it 'should create an instance of ImportReferenceRequestSchema' do
      expect(@instance).to be_instance_of(RusticiSoftwareCloudV2::ImportReferenceRequestSchema)
    end
  end
  describe 'test attribute "url"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "web_path_to_course"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end
