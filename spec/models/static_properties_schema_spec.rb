=begin
#Rustici Engine API

#Rustici Engine API

OpenAPI spec version: 2.0

Generated by: https://openapi-generator.tech
OpenAPI Generator version: 4.0.0-beta3

=end

require 'spec_helper'
require 'json'
require 'date'

# Unit tests for RusticiSoftwareCloudV2::StaticPropertiesSchema
# Automatically generated by openapi-generator (https://openapi-generator.tech)
# Please update as you see appropriate
describe 'StaticPropertiesSchema' do
  before do
    # run before each test
    @instance = RusticiSoftwareCloudV2::StaticPropertiesSchema.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of StaticPropertiesSchema' do
    it 'should create an instance of StaticPropertiesSchema' do
      expect(@instance).to be_instance_of(RusticiSoftwareCloudV2::StaticPropertiesSchema)
    end
  end
  describe 'test attribute "completion_threshold"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "launch_data"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "max_time_allowed"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "scaled_passing_score"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "scaled_passing_score_used"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "time_limit_action"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end
