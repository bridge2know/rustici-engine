=begin
#Rustici Engine API

#Rustici Engine API

OpenAPI spec version: 2.0

Generated by: https://openapi-generator.tech
OpenAPI Generator version: 4.0.0-beta3

=end

require 'spec_helper'
require 'json'
require 'date'

# Unit tests for RusticiSoftwareCloudV2::TitleSchema
# Automatically generated by openapi-generator (https://openapi-generator.tech)
# Please update as you see appropriate
describe 'TitleSchema' do
  before do
    # run before each test
    @instance = RusticiSoftwareCloudV2::TitleSchema.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of TitleSchema' do
    it 'should create an instance of TitleSchema' do
      expect(@instance).to be_instance_of(RusticiSoftwareCloudV2::TitleSchema)
    end
  end
  describe 'test attribute "title"' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end
